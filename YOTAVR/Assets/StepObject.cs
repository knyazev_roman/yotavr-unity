﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StepObject : MonoBehaviour {
    public bool Finish = false;
    /// <summary>
    /// Скорость анимаии
    /// </summary>
    public float speed = 0.173f;
    /// <summary>
    /// UI который будет скрываться после выполнения кветса
    /// </summary>
    public Image[] UiHides;
    /// <summary>
    /// Рендер спрайтов
    /// </summary>
    public SpriteRenderer[] UISprites;
    /// <summary>
    /// Этап закончился
    /// </summary>
    public bool IsDone = false;
    /// <summary>
    /// Показыть анимацию появляения
    /// </summary>
    bool isAnim = false;
    /// <summary>
    /// Итератор времени
    /// </summary>
    float t = 1;
    void Update () {
        if (isAnim){
            t = t + (speed * Time.deltaTime);
            if(UiHides!=null)
            {
                foreach (var item in UiHides)
                {
                    item.GetComponent<Image>().color = new Color(
                    item.GetComponent<Image>().color.r,
                    item.GetComponent<Image>().color.g,
                    item.GetComponent<Image>().color.b,
                    t
                    );
                }
            }
            if(UISprites!=null)
            {
                foreach (var item in UISprites)
                {
                    item.GetComponent<SpriteRenderer>().color = new Color(
                    item.GetComponent<SpriteRenderer>().color.r,
                    item.GetComponent<SpriteRenderer>().color.g,
                    item.GetComponent<SpriteRenderer>().color.b,
                    t
                    );
                }
            }
            if(t>=1)
                isAnim = false;
        }
        if(IsDone){
            t = t - (speed * Time.deltaTime);
            if(UiHides!=null)
            {
                foreach (var item in UiHides)
                {
                    item.GetComponent<Image>().color = new Color(
                    item.GetComponent<Image>().color.r,
                    item.GetComponent<Image>().color.g,
                    item.GetComponent<Image>().color.b,
                    t
                    );
                }
            }
            if (UISprites != null)
            {
                foreach (var item in UISprites)
                {
                    item.GetComponent<SpriteRenderer>().color = new Color(
                    item.GetComponent<SpriteRenderer>().color.r,
                    item.GetComponent<SpriteRenderer>().color.g,
                    item.GetComponent<SpriteRenderer>().color.b,
                    t
                    );
                }
            }
            if (t <= 0) Finish = true;
        }
    }
    /// <summary>
    /// Показать UI потихоньку рендерить
    /// </summary>
    public void Show()
    {
        isAnim = true;
        HideUI();
        t = 0;
    }
    /// <summary>
    /// Скрыть UI моментально
    /// </summary>
    public void HideUI()
    {
        if(UiHides!=null)
        {
            foreach (var item in UiHides)
            {
                item.GetComponent<Image>().color = new Color(
                item.GetComponent<Image>().color.r,
                item.GetComponent<Image>().color.g,
                item.GetComponent<Image>().color.b,
                0
                );
            }
        }
        if (UISprites != null)
        {
            foreach (var item in UISprites)
            {
                item.GetComponent<SpriteRenderer>().color = new Color(
                item.GetComponent<SpriteRenderer>().color.r,
                item.GetComponent<SpriteRenderer>().color.g,
                item.GetComponent<SpriteRenderer>().color.b,
                0
                );
            }
        }
    }
}
