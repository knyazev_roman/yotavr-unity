﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AfterStart : MonoBehaviour {

    public AudioSource sourceActive;
    public float timer = 3;

	// Use this for initialization
	void Start () {
		
	}

    bool t = false;
	// Update is called once per frame
	void Update () {
		if(t==false)
        {
            timer -= Time.deltaTime;
            if(timer<=0){
                t = true;
                sourceActive.enabled = true;
            }
        }
	}
}
