﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;

public class VREyeRaycaster : MonoBehaviour {

    public bool m_ShowDebugRay = false;
    public float m_RayLength = 1000f;
    public LayerMask m_ExclusionLayers;           // Layers to exclude from the raycast.



    public float m_DebugRayLength = 5f;
    public float m_DebugRayDuration = 1f;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000f))
        {
            if (Input.GetButton("Tap")){
                if (hit.transform.gameObject.GetComponent<ItemConfirm>() != null)
                {
                    hit.transform.gameObject.GetComponent<ItemConfirm>().Loadlevel();
                }
            }
            //if (hit.collider.CompareTag("Exit"))
            //    return;
            //TimerLeft = Timer;
            //print(hit.transform.gameObject.name);
        }
        // Fire ray from camera
        //float rayLength = 200f;
        //Ray ray = Camera.main.ScreenPointToRay(this.transform.position);
        //RaycastHit hit;
        //// If ray hits object within length
        //if (Physics.Raycast(ray, out hit, rayLength)){
        //    Debug.Log("Collision detected!");
        //}
        // EyeRaycast();
        //RaycastHit[] hitsList = Physics.RaycastAll(Camera.main.transform.forward, 1000, m_ExclusionLayers);
        //print(hitsList.Length);
        //RaycastHit hit;
        //if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, Mathf.Infinity))
        //{
        //    Debug.Log("Collider detected! 3D collider works!");
        //}
    }

    private void EyeRaycast()
    {
        // Show the debug ray if required
        if (m_ShowDebugRay)
        {
            Debug.DrawRay(this.transform.position, this.transform.forward * 1000, Color.blue, m_DebugRayDuration);
        }

        //Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        //RaycastHit hit;
        //if (Physics.Raycast(ray, out hit, 1000f, mask))

        Ray ray = new Ray(this.transform.position, this.transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000f, m_ExclusionLayers)){
            if (Input.GetButton("Tap")){
                if (hit.transform.gameObject.GetComponent<ItemConfirm>() != null){
                    hit.transform.gameObject.GetComponent<ItemConfirm>().Loadlevel();
                }
            }
        }

        // Create a ray that points forwards from the camera.
        //Ray ray = new Ray(this.transform.position, this.transform.forward);
        //RaycastHit hit;

        //// Do the raycast forweards to see if we hit an interactive item
        //if (Physics.Raycast(ray, out hit, m_RayLength, ~m_ExclusionLayers))
        //{
        //    print("work");
        //    if (hit.transform.gameObject.GetComponent<UnityEngine.UI.Button>()!=null)
        //    {
        //        //if(Input.Button)
        //        print("work");
        //    }
        //    //VRInteractiveItem interactible = hit.collider.GetComponent<VRInteractiveItem>(); //attempt to get the VRInteractiveItem on the hit object
        //    //m_CurrentInteractible = interactible;

        //    //// If we hit an interactive item and it's not the same as the last interactive item, then call Over
        //    //if (interactible && interactible != m_LastInteractible)
        //    //    interactible.Over();

        //    //// Deactive the last interactive item 
        //    //if (interactible != m_LastInteractible)
        //    //    DeactiveLastInteractible();

        //    //m_LastInteractible = interactible;

        //    //// Something was hit, set at the hit position.
        //    //if (m_Reticle)
        //    //    m_Reticle.SetPosition(hit);

        //    //if (OnRaycasthit != null)
        //    //    OnRaycasthit(hit);
        //}
        //else
        //{
        //    // Nothing was hit, deactive the last interactive item.
        //    DeactiveLastInteractible();
        //    m_CurrentInteractible = null;

        //    // Position the reticle at default distance.
        //    if (m_Reticle)
        //        m_Reticle.SetPosition();
        //}
    }


}
