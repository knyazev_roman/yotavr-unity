﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorViewer : MonoBehaviour {

    GvrHead head;
    Vector3 rotation;

    void Start()
    {
        head = FindObjectOfType<GvrHead>();     
    }

	void Update () {

			
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            if (Cursor.lockState == CursorLockMode.Locked)
                Cursor.lockState = CursorLockMode.None;
            else
                Cursor.lockState = CursorLockMode.Locked;
        }
        rotation.x -= Input.GetAxis("Mouse Y");
        rotation.y += Input.GetAxis("Mouse X");
        head.transform.rotation = Quaternion.Euler(rotation);
#endif
	}
}
