﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GirlsManager : MonoBehaviour {
    public GirlFynny[] girls;
    public LayerMask mask;

    // Use this for initialization
    void Start () {
        GetComponent<Animation>().GetClip("Girl").wrapMode = WrapMode.Once;
	}

    bool isHoverActivate = false;
	// Update is called once per frame
	void Update () {
        if (!isHoverActivate)
        {
            Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 2000f, mask)){
                if (hit.transform.gameObject.GetComponent<GirlFynny>() != null){
                    hit.transform.gameObject.GetComponent<MediaPlayerCtrl>().Play();
                    isHoverActivate = true;
                }
            }
        }
        else
        {

        }
    }
}
