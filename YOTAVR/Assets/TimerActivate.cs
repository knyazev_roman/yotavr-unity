﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerActivate : MonoBehaviour {

    public float TimerActivateObject = 10;
    public GameObject ObjectActivate;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(TimerActivateObject>=0)
        {
            TimerActivateObject -= Time.deltaTime;
            if(TimerActivateObject<=0)
            {
                ObjectActivate.SetActive(true);
            }
        }
    }
}
