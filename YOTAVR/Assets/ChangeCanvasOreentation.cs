﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCanvasOreentation : MonoBehaviour {
    public GameObject CanvasPortrait;
    public GameObject CanvasLandScape;
    // Use this for initialization
    void Start () {
		
	}
	void Update () {
        if (Screen.orientation == ScreenOrientation.Portrait)
        {
            CanvasPortrait.SetActive(true);
            CanvasLandScape.SetActive(false);
        }
        else
        {
            CanvasPortrait.SetActive(false);
            CanvasLandScape.SetActive(true);
        }
	}
}
