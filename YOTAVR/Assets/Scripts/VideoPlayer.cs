﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoPlayer : MonoBehaviour {


    public static Vector3 StartRoratation = Vector3.zero;

    private static AsyncOperation operation;
    void Start () {
        StartCoroutine(LoadScene());
    }
    private IEnumerator LoadScene()
    {
        operation = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync((int)ScenesData.SCENES.MAIN/*, UnityEngine.SceneManagement.LoadSceneMode.Single*/);
        operation.allowSceneActivation = false;
        yield return operation;
    }
    public float timeLeft = 13;
    void Update () {
        timeLeft -= Time.deltaTime;
        if (timeLeft < 0){
            
            Invoke("Loaded", 0);
        }
    }
    private void Loaded()
    {
        
        operation.allowSceneActivation = true;
    }

    IEnumerator FadeScene()
    {
        print("load");
        float fadeTime = FindObjectOfType<Fading>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime);
        
        //SceneManager.LoadScene((int)Scene);
    }
}
