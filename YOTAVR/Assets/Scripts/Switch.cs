﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Switch : MonoBehaviour {

    public ScenesData.SCENES Scene;

    // Use this for initialization
    void Start () {
		
	}

    public void Done(int value)
    {
        SceneManager.LoadScene((int)Scene);
    }
}
