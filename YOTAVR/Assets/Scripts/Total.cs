﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Total : MonoBehaviour {

    public Text Minutes;
    public Text GB;

    /*public GameObject SMS;
    public GameObject Unlimited;*/

    public GameObject UnlimSMSChecked;
    public GameObject UnlimChecked;
    public GameObject SMSChecked;

    public ShowTotalPrice Price3;
    public ShowTotalPrice Price4;

    // Use this for initialization
    void Start () {

        Minutes.text = UserData.TarrifMinute.ToString();
        GB.text = UserData.TarrifGB.ToString();
        int sum = UserData.TarrifTotalSum;
        //sum += UserData.TarrifTotalSum;

        if(UserData.SMS == 1 && UserData.UNLIMITED == 1)
        {
            UnlimSMSChecked.SetActive(true);
            //sum += 200;
            if (UserData.City == "Москва")
            {
                sum += 150;
            }
            else
            {
                sum += 100;
            }
            if (UserData.SMS == 1)
            {
                sum += 50;
            }
        }
        else if (UserData.SMS == 1)
        {
            SMSChecked.SetActive(true);
            sum += 50;
        }
        else if (UserData.UNLIMITED == 1)
        {
            UnlimChecked.SetActive(true);
            if(UserData.City == "Москва")
            {
                sum += 150;
            }
            else
            {
                sum += 100;
            }
        }
        print(sum);
        if (sum < 1000)
        {
            Price3.Price = sum;
            Price3.gameObject.SetActive(true);
            Price4.gameObject.SetActive(false);
        }
        else
        {
            Price4.Price = sum;
            Price4.gameObject.SetActive(true);
            Price3.gameObject.SetActive(false);
        }
        	
	}
	
}
