﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnableVR : MonoBehaviour {

    public bool IsDone;
    public Text TEST;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        TEST.text = Screen.orientation.ToString();
        if (IsDone)
            return;
        //if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft)
        if(Screen.orientation == ScreenOrientation.Landscape)
        {
            TEST.text = "DONE";
            IsDone = true;
            FindObjectOfType<Manager>().SetVR();
            FindObjectOfType<TimerLoadScene>().Wait = false; 
        }
    }
}
