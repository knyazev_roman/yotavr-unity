﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapMaterialgenerator : MonoBehaviour {

    public bool GenerateMaterials;
    public bool HideGroup;
    public bool ShowGroup;
    public List<Material> Materials;

	// Use this for initialization
	void Start () {
        
        for(int i=0; i< transform.childCount; i++)
        {
            var parent = transform.GetChild(i);
            
            for (int j=0; j< parent.childCount; j++)
            {
                var group = parent.GetChild(j);
                var item = group.GetChild(0);
                if (GenerateMaterials)
                {
                    for (int m = 0; m < item.childCount; m++)
                    {
                        var temp = item.GetChild(m);
                        temp.GetComponent<MeshRenderer>().material = Materials[Random.Range(0, Materials.Count - 1)];
                        //print(t);
                    }
                }
                if (HideGroup)
                {
                    item.gameObject.SetActive(false);
                }

                if (ShowGroup)
                {
                    item.gameObject.SetActive(true);
                }
                //
                //print(a);
            }
            
        }
        //List<GameObject> rootItem = transform.childCount;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
