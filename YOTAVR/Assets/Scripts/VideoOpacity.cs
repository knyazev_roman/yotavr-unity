﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoOpacity : MonoBehaviour {

    public MeshRenderer mesh;
    public Material mat;

	// Use this for initialization
	void Start () {
        mat = mesh.materials[0];
	}
	
	// Update is called once per frame
	void Update () {
        var color = mat.color;
        color.a -= 0.5f;
        mat.color = color;
	}
}
