﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UndoYota : MonoBehaviour {

    public LayerMask mask;
    public float FillSpeed = 1f;
    public GameObject HandleLoader;
 
    public GameObject LookingFor;
    private float fillAmount;
    private bool IsShown;
    public List<PhoneItem> PhoneItems;

    public GameObject sendButton;

    // Use this for initialization
    void Start () {
        PhoneItems = FindObjectOfType<PhoneData>().ItemPhone;
    }
	
	// Update is called once per frame
	void Update () {
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000f, mask))
        {
            if (hit.collider.gameObject != LookingFor)
                return;
            //if (drag)
            //    return;
            IsShown = true;
            fillAmount += Time.deltaTime * FillSpeed;
            HandleLoader.GetComponent<Image>().fillAmount = fillAmount / 100;
            if (fillAmount >= 100 || Input.GetButtonDown("Tap")) // Удаление цифры
            {
                for (int i = PhoneItems.Count - 1; i >= 0; i--)
                {
                    if (PhoneItems[i].filled)
                    {
                        PhoneItems[i].Character = null;
                        break;
                    }
                }

                reset();
            }
            //print(fillAmount);
        }
        else if (IsShown)
        {
            reset();
        }
    }

    private void RemoveNumber()
    {
        for(int i = PhoneItems.Count - 1; i >= 0; i--)
        {
            if (PhoneItems[i].filled)
            {
                PhoneItems[i].reset();
            }
        }
    }

    private void reset()
    {
        IsShown = false;
        fillAmount = 0;
        HandleLoader.GetComponent<Image>().fillAmount = 0;
    }
}
