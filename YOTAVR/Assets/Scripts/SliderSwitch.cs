﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SliderSwitch : MonoBehaviour {

    public LayerMask mask;
    public float FillSpeed = 1f;
    private float fillAmount;
    public GameObject HandleLoader;
    public GameObject LookingFor;
    public GameObject AnotherObject;
    public Animation animationOn;
    public Animation animationOff;
    public bool IsOn;
    public bool IsClickEvent = true;
    public float timerTouch = 0.5f;

    AudioSource s;
    public AudioClip clipSwitch;
    public AudioClip clipSwitchSet;
    // Use this for initialization
    void Start () {
        //animationOn["SwitcherOff"].speed = 0;
        //animationOn.Play("SwitcherOff");

        //animationOff["Switcher"].speed = 0;
        //animationOff.Play("Switcher");

        s = this.gameObject.AddComponent<AudioSource>();
        
    }

    
	
	// Update is called once per frame
	void Update () {
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000f, mask))
        {
            //print(hit.transform.name);
            if (hit.collider.gameObject != LookingFor)
                return;
            if (!IsClickEvent)
            {
                fillAmount += Time.deltaTime * FillSpeed;
                HandleLoader.GetComponent<Image>().fillAmount = fillAmount / 100;
                if (fillAmount >= 100)
                {
                    Switch();
                }
            }
            else
            {
                Switch();
            }
            
        }
        else 
        {
            if(!IsClickEvent)
                reset();
        }
    }

    void Switch()
    {
        bool detected = false;
        timerTouch += Time.deltaTime;
#if UNITY_EDITOR
        if (Input.GetMouseButton(0) && timerTouch > 1)
        {
            timerTouch = 0;
            detected = true;
            //print("1");
            //s.PlayOneShot(clipSwitch);
        }
#endif
        if (!detected && Input.GetMouseButtonDown(0))
        {
            timerTouch = 0;
            detected = true;
            //print("2");
            //s.PlayOneShot(clipSwitch);
        }
        if (!detected && Input.touchCount > 0 && timerTouch > 1)
        {
            timerTouch = 0;
            detected = true;

            s.PlayOneShot(clipSwitch);
        }
        bool doneEvent = true;
        if (IsClickEvent && !detected)
            doneEvent = false;
        if (!doneEvent)
            return;
        print("UU: " + UserData.UNLIMITED + " IsOn: " + IsOn);
        if (UserData.UNLIMITED == 0 && !IsOn || UserData.UNLIMITED == 1 && IsOn)
        {
            FindObjectOfType<TimerLoadScene>().Wait = false;
            FindObjectOfType<TimerLoadScene>().timeLeft = 0;
        }
        else
        {
            if (!IsOn)
            {
                //animationOn["SwitcherOff"].speed = 1;
                UserData.UNLIMITED = 0;
            }
            else
            {
                //animationOff["Switcher"].speed = 1;
                UserData.UNLIMITED = 1;
            }
            gameObject.layer = 0;
            //AnotherObject.layer = 0;
            FindObjectOfType<TimerLoadScene>().timeLeft = 0;
            FindObjectOfType<TimerLoadScene>().Wait = false;
        }
    }

    private void reset()
    {
        fillAmount = 0;
        HandleLoader.GetComponent<Image>().fillAmount = 0;
    }
}
