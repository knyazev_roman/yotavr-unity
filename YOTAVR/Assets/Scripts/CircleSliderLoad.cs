﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CircleSliderLoad : MonoBehaviour {

    public LayerMask mask;
    public float FillSpeed = 1f;
    private float fillAmount;
    public GameObject HandleLoader;
    public GameObject LookingFor;
    public ScenesData.SCENES Scene;
    public SliderEvent MyEvent = SliderEvent.NONE;
    public bool IsClickEvent = true;
    public float timerTouch = 0.5f;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000f, mask))
        {
            if (hit.collider.gameObject != LookingFor)
                return;
            if (!IsClickEvent)
            {
                fillAmount += Time.deltaTime * FillSpeed;
                HandleLoader.GetComponent<Image>().fillAmount = fillAmount / 100;
                if (fillAmount >= 100)
                {
                    Event();
                }
            }
            else
            {
                Event();
            }
            
            //print(fillAmount);
        }
        else 
        {
            if(!IsClickEvent)
                reset();
        }
    }

    private void Event()
    {
        bool detected = false;
        timerTouch += Time.deltaTime;
#if UNITY_EDITOR
        if (Input.GetMouseButton(0) && timerTouch > 1)
        {
            timerTouch = 0;
            detected = true;
            //print("1");
        }
#endif
        if (!detected && Input.GetMouseButtonDown(0))
        {          
            timerTouch = 0;
            detected = true;
            //print("2");
        }
        if (!detected && Input.touchCount > 0 && timerTouch > 1)
        {
            timerTouch = 0;
            detected = true;
            //print("3");
        }
        bool doneEvent = true;
        if (IsClickEvent && !detected)
            doneEvent = false;
        if (!doneEvent)
            return;
        switch (MyEvent)
        {
            case SliderEvent.TARRIF:
                var _curveSliderTarif = GameObject.FindGameObjectWithTag("Tarrif").GetComponent<CurveSlider>();
                UserData.Tarrif = _curveSliderTarif.SliderValue.MINSum;
                UserData.TarrifMinSum = _curveSliderTarif.SliderValue.MINSum;
                //UserData.TarrifGB = _curveSliderTarif.SliderValue.GB;
                UserData.TarrifMinute = _curveSliderTarif.SliderValue.Minutes;
                //UserData.TarrifSum = _curveSliderTarif.SliderValue.MINSum;
                print(UserData.Tarrif);
                break;

            case SliderEvent.GB:
                var _curveSliderGB = GameObject.FindGameObjectWithTag("Tarrif").GetComponent<CurveSlider>();
                UserData.TarrifGBPrice = _curveSliderGB.SliderValue.GBSumm;
                UserData.TarrifGB = _curveSliderGB.SliderValue.GB;
                //UserData.TarrifMinute = _curveSliderGB.SliderValue.Minutes;
                //UserData.TarrifGBPrice = _curveSliderGB.SliderValue.GBSumm;
                print(UserData.Tarrif);
                break;

            case SliderEvent.SMS:
                UserData.SMS = 1;
                break;

            case SliderEvent.NO_SMS:
                UserData.SMS = 0;
                break;

            case SliderEvent.UNLIMITED:
                UserData.UNLIMITED = 1;
                break;

            case SliderEvent.NO_UNLIMITED:
                UserData.UNLIMITED = 0;
                break;

            case SliderEvent.PHONE:
                var items = FindObjectOfType<PhoneData>().ItemPhone;
                for (int i = 0; i < items.Count; i++)
                {
                    for (int j = 0; j < items.Count - 1; j++)
                    {
                        if (items[j].position > items[j + 1].position)
                        {
                            var temp = items[j];
                            items[j] = items[j + 1];
                            items[j + 1] = temp;
                        }
                    }
                }
                string phone = "";
                foreach (var item in items)
                {
                    phone += item.Character;
                }
                UserData.PHONE = phone;
                print(phone);
                break;
        }
        print("done");
        SceneManager.LoadScene((int)Scene);
    }

    private void reset()
    {
        fillAmount = 0;
        HandleLoader.GetComponent<Image>().fillAmount = 0;
    }
}
