﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class CompasNextButton : MonoBehaviour {

    public LayerMask mask;
    public GameObject LookingFor;
    public ScenesData.SCENES Scene;
    public Animation anim;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if (!anim.isPlaying) anim.Play();

		Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000f, mask))
        {
            if (!Input.GetButton("Tap")) return;
            if (hit.collider.gameObject != LookingFor)
                return;

            SceneManager.LoadScene((int)Scene);
        }
	}
}
