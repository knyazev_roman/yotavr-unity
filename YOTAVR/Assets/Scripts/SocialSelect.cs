﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class SocialSelect : MonoBehaviour
{
    public LayerMask mask;
    public GameObject LookingFor;
    private bool IsShown;
    public SocialEvent SocialEvent;
    public Text TotalPrice;

    public Sprite EnabledSprite, DisabledSprite, HoveredSprite;
    public GameObject CostImageSocial, CostImageMessage, CheckImage;

    public List<GameObject> CostImageSocialList;
    public bool isSocialOrMess;

    private Vector3 StartPosition;
    public float RandomSpeed = 3;

    private bool _buttonEnabled = false;
    private Image _buttonImage;
    private int _active = 0;
    int _sum = 0;
    bool yTurn = true;
    //
    //	bool floatAngle = true;
    //
    //	float AngleRot = 0;
    public bool IsClickEvent = true;
    AudioSource s;
    AudioClip sSelect;

    int Social;
    int Mess;

    float TimeLeft = 120;
    bool isTime = false;


    // Use this for initialization
    void Start()
    {
        Social = UserData.MyCity[0].Social;
        Mess = UserData.MyCity[0].Message;
        if (isSocialOrMess)
        {
            if (Social == 20)
            {
                CostImageSocial = CostImageSocialList[0];
            }
            else if (Social == 25)
            {
                CostImageSocial = CostImageSocialList[1];
            }
        }
        else
        {
            if (Mess == 10)
            {
                CostImageMessage = CostImageSocialList[0];
            }
            else if (Social == 25)
            {
                CostImageMessage = CostImageSocialList[1];
            }
        }


        CheckValue();
        TotalPrice.text = _sum.ToString();
        StartPosition = this.transform.position;
        RandomSpeed = Random.Range(3, 7);
        s = this.gameObject.AddComponent<AudioSource>();
    }

    public int delay = 20;

    // Update is called once per frame
    void Update()
    {
        if (isTime)
        {
            if (TimeLeft >= 0)
                TimeLeft -= 1f;
            Debug.Log(TimeLeft);
        }
        else
        {
            TimeLeft = delay;
            isTime = false;
        }
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000f, mask))
        {

            if (hit.collider.gameObject == LookingFor)
            {
                AddSocial();
            }
        }
        else
        {
            _buttonImage = LookingFor.GetComponent<Image>();
            if (isSocialOrMess) CostImageSocial.SetActive(false);
            else CostImageMessage.SetActive(false);
            if (_buttonEnabled)
            {
                _buttonImage.sprite = HoveredSprite;
            }
            else
            {
                _buttonImage.sprite = DisabledSprite;
            }
        }
        SmallTurn();
    }

    void AddSocial()
    {
        bool detected = false;
#if UNITY_EDITOR
        if (Input.GetMouseButton(0))
        {
            if (TimeLeft == delay)
            {
                SwapImage();
                SetPrice();
                detected = true;
                print("1");
                isTime = true;
            }
            else
            {
                if (TimeLeft <= 0)
                    isTime = false;
            }

        }
#endif
        if (!detected && Input.GetMouseButtonDown(0))
        {
            detected = true;
            print("2");
        }

        if (!detected && Input.touchCount > 0)
        {
            if (TimeLeft == delay)
            {
                detected = true;
                SwapImage();
                SetPrice();
                isTime = true;
            }
            else
            {
                if (TimeLeft <= 0)
                    isTime = false;
            }
        }
        else
        {
            _buttonImage = LookingFor.GetComponent<Image>();
            _buttonImage.sprite = HoveredSprite;
            if (isSocialOrMess) CostImageSocial.SetActive(true);
            else CostImageMessage.SetActive(true);
        }

        bool doneEvent = true;
        if (IsClickEvent && !detected)
            doneEvent = false;
        if (!doneEvent)
            return;
        IsShown = true;
    }

    void CheckValue()
    {
        if (SocialEvent == SocialEvent.FACEBOOK) { _buttonEnabled = (UserData.FACEBOOK == 1); }
        if (SocialEvent == SocialEvent.INSTAGRAM) { _buttonEnabled = (UserData.INSTAGRAM == 1); }
        //if (SocialEvent == SocialEvent.MESSAGE) { _buttonEnabled = (UserData.MESSAGE == 1); }
        if (SocialEvent == SocialEvent.OK) { _buttonEnabled = (UserData.OK == 1); }
        if (SocialEvent == SocialEvent.SKYPE) { _buttonEnabled = (UserData.SKYPE == 1); }
        if (SocialEvent == SocialEvent.SMS) { _buttonEnabled = (UserData.SMS == 1); }
        if (SocialEvent == SocialEvent.TELEGRAM) { _buttonEnabled = (UserData.TELEGRAM == 1); }
        if (SocialEvent == SocialEvent.TWITTER) { _buttonEnabled = (UserData.TWITTER == 1); }
        if (SocialEvent == SocialEvent.VIBER) { _buttonEnabled = (UserData.VIBER == 1); }
        if (SocialEvent == SocialEvent.VK) { _buttonEnabled = (UserData.VK == 1); }
        if (SocialEvent == SocialEvent.WHATSAPP) { _buttonEnabled = (UserData.WHATSAPP == 1); }
        CheckImage.SetActive(_buttonEnabled);
        SetPrice();
    }

    public void SetPrice()
    {
        _sum = UserData.TarrifMinSum;

        if (UserData.FACEBOOK == 1) { _sum += Social; UserData.MESSAGE = 1; }
        else
        {
            UserData.MESSAGE = 0;
        }
        if (UserData.INSTAGRAM == 1) { _sum += Social; }
        //if (UserData.MESSAGE == 1) { _sum += 10; }
        if (UserData.OK == 1) { _sum += Social; }
        if (UserData.SKYPE == 1) { _sum += Mess; }
        //if (UserData.SMS == 1) { _sum += 50; }
        if (UserData.TELEGRAM == 1) { _sum += Mess; }
        if (UserData.TWITTER == 1) { _sum += Social; }
        if (UserData.VIBER == 1) { _sum += Mess; }
        if (UserData.VK == 1) { _sum += Social; }
        if (UserData.WHATSAPP == 1) { _sum += Mess; }
        UserData.Social = _sum;
        UserData.TarrifTotalSum = _sum;
    }

    void SwapImage()
    {
        _buttonImage = LookingFor.GetComponent<Image>();
        if (_buttonEnabled)
        {
            _buttonEnabled = false;
            _buttonImage.sprite = DisabledSprite;
            SelectSocial(false);
        }
        else
        {
            _buttonEnabled = true;
            _buttonImage.sprite = HoveredSprite;
            SelectSocial(true);
        }
        SetPrice();
        UserData.TarrifTotalSum = _sum;
        TotalPrice.text = _sum.ToString();
        CheckImage.SetActive(_buttonEnabled);
    }

    void SelectSocial(bool enabled)
    {
        if (enabled)
        {
            _active = 1;
        }
        else
        {
            _active = 0;
        }
        switch (SocialEvent)
        {
            case SocialEvent.FACEBOOK:
                UserData.FACEBOOK = _active;
                UserData.MESSAGE = _active;
                break;
            case SocialEvent.INSTAGRAM:
                UserData.INSTAGRAM = _active;
                break;
            case SocialEvent.OK:
                UserData.OK = _active;
                break;
            case SocialEvent.SKYPE:
                UserData.SKYPE = _active;
                break;
            case SocialEvent.SMS:
                UserData.SMS = _active;
                break;
            case SocialEvent.TELEGRAM:
                UserData.TELEGRAM = _active;
                break;
            case SocialEvent.TWITTER:
                UserData.TWITTER = _active;
                break;
            case SocialEvent.VIBER:
                UserData.VIBER = _active;
                break;
            case SocialEvent.VK:
                UserData.VK = _active;
                break;
            case SocialEvent.WHATSAPP:
                UserData.WHATSAPP = _active;
                break;

        }

    }

    void SmallTurn()
    {
        //		//return;
        //		if (IsShown)
        //			return;
        //		reset();
        if (yTurn)
        {
            if (transform.position.y <= (StartPosition.y + 5))
                transform.position += new Vector3(0, RandomSpeed * Time.deltaTime, 0);
            else
            {
                yTurn = false;
            }
        }
        else
        {
            if (transform.position.y >= (StartPosition.y - 5))
                transform.position -= new Vector3(0, RandomSpeed * Time.deltaTime, 0);
            else
            {
                yTurn = true;
            }
        }


    }

    private void reset()
    {
        IsShown = false;

        //HandleLoader.GetComponent<Image>().fillAmount = 0;
    }
}
