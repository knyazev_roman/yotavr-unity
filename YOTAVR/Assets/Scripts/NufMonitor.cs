﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NufMonitor : MonoBehaviour {

    DrawPoint dp;
    public SpriteRenderer Text;
    public AudioClip SoundFinish;
    AudioSource SourceAudio;
    public GameObject TextFinishPart;

	// Use this for initialization
	void Start () {
        dp = this.GetComponent<DrawPoint>();
        Text.gameObject.SetActive(false);

        SourceAudio = this.GetComponent<AudioSource>();
    }
    public float timeLeft = 5f;
    bool showIsDone = false;
	// Update is called once per frame
	void Update () {
        if (!showIsDone)
        {
            if (dp.IsDone)
            {
                showIsDone = true;
                Text.gameObject.SetActive(true);
                TextFinishPart.gameObject.SetActive(true);
                if (SoundFinish != null){
                    SourceAudio.Stop();
                    SourceAudio.PlayOneShot(SoundFinish);
                }
            }
        }
        else
        {
            timeLeft -= Time.deltaTime;
            if (timeLeft < 0){
                //dp.FastLoad = true;
                this.enabled = false;
                FindObjectOfType<DrawPoint>().CallDestroy();
            }
        }
	}
}
