﻿using System.Collections.Generic;
using UnityEngine;

public class UserData
{


    public static List<string> yotaCodePassCities = new List<string>()
    {
        /*
        "Москва",
        "Санкт-Петербург",
        "Нижний Новгород"
        */
    };

    private static string cityKey = "city";
    private static string tarrifKey = "tarrif";
    private static string tarrifSumKey = "tarrifSum";
    private static string tarrifMinutesKey = "tarrifMinutes";
    private static string tarrifGBKey = "tarrifGB";
    private static string tarrifTotalSumKey = "tarrifTotalSum";
    private static string tarrifGBPriceKey = "tarrifPriceGB";
    private static string smsKey = "sms";
    private static string unlimitedKey = "unlimited";
    private static string skypeKey = "skype";
    private static string messageKey = "message";
    private static string whatsAppKey = "whatsApp";
    private static string facebookKey = "facebook";
    private static string vkKey = "vk";
    private static string instagramKey = "instagram";
    private static string twitterKey = "twitter";
    private static string viberKey = "viber";
    private static string telegramKey = "telegram";
    private static string OKKey = "ok";
    private static string social = "social";
    private static string tarrifMinSumm = "tarrifMinSumm";


    private static string phoneKey = "phone";
    private static string promoKey = "promo";
    private static string firstLoadKey = "firstLoad";

    public static int IsFirstLoad
    {
        get { return PlayerPrefs.GetInt(firstLoadKey); }
        set
        {
            PlayerPrefs.SetInt(firstLoadKey, value);
            SaveData();
        }
    }

    public static int Social
    {
        get { return PlayerPrefs.GetInt(social); }
        set
        {
            PlayerPrefs.SetInt(social, value);
            SaveData();
        }
    }

    public static string City
    {
        get { return PlayerPrefs.GetString(cityKey); }
        set
        {
            PlayerPrefs.SetString(cityKey, value);
            SaveData();
        }
    }

    public static int Tarrif
    {
        get { return PlayerPrefs.GetInt(tarrifKey); }
        set
        {
            PlayerPrefs.SetInt(tarrifKey, value);
            SaveData();
        }
    }

    public static int TarrifGBPrice
    {
        get { return PlayerPrefs.GetInt(tarrifGBPriceKey); }
        set
        {
            PlayerPrefs.SetInt(tarrifGBPriceKey, value);
            SaveData();
        }
    }


    public static int TarrifMinSum
    {
        get { return PlayerPrefs.GetInt(tarrifMinSumm); }
        set
        {
            PlayerPrefs.SetInt(tarrifMinSumm, value);
            SaveData();
        }
    }

    public static int TarrifSum
    {
        get { return PlayerPrefs.GetInt(tarrifSumKey); }
        set
        {
            PlayerPrefs.SetInt(tarrifSumKey, value);
            SaveData();
        }
    }

    public static int TarrifMinute
    {
        get { return PlayerPrefs.GetInt(tarrifMinutesKey); }
        set
        {
            PlayerPrefs.SetInt(tarrifMinutesKey, value);
            SaveData();
        }
    }

    public static int TarrifGB
    {
        get { return PlayerPrefs.GetInt(tarrifGBKey); }
        set
        {
            PlayerPrefs.SetInt(tarrifGBKey, value);
            SaveData();
        }
    }

    public static int TarrifTotalSum
    {
        get { return PlayerPrefs.GetInt(tarrifTotalSumKey); }
        set
        {
            PlayerPrefs.SetInt(tarrifTotalSumKey, value);
            SaveData();
        }
    }

    public static int SMS
    {
        get { return PlayerPrefs.GetInt(smsKey); }
        set
        {
            PlayerPrefs.SetInt(smsKey, value);
            SaveData();
        }
    }

    public static int UNLIMITED
    {
        get { return PlayerPrefs.GetInt(unlimitedKey); }
        set
        {
            PlayerPrefs.SetInt(unlimitedKey, value);
            SaveData();
        }
    }

    public static int SKYPE
    {
        get { return PlayerPrefs.GetInt(skypeKey); }
        set
        {
            PlayerPrefs.SetInt(skypeKey, value);
            SaveData();
        }
    }

    public static int MESSAGE
    {
        get { return PlayerPrefs.GetInt(messageKey); }
        set
        {
            PlayerPrefs.SetInt(messageKey, value);
            SaveData();
        }
    }

    public static int WHATSAPP
    {
        get { return PlayerPrefs.GetInt(whatsAppKey); }
        set
        {
            PlayerPrefs.SetInt(whatsAppKey, value);
            SaveData();
        }
    }

    public static int FACEBOOK
    {
        get { return PlayerPrefs.GetInt(facebookKey); }
        set
        {
            PlayerPrefs.SetInt(facebookKey, value);
            SaveData();
        }
    }

    public static int VK
    {
        get { return PlayerPrefs.GetInt(vkKey); }
        set
        {
            PlayerPrefs.SetInt(vkKey, value);
            SaveData();
        }
    }

    public static int INSTAGRAM
    {
        get { return PlayerPrefs.GetInt(instagramKey); }
        set
        {
            PlayerPrefs.SetInt(instagramKey, value);
            SaveData();
        }
    }

    public static int TWITTER
    {
        get { return PlayerPrefs.GetInt(twitterKey); }
        set
        {
            PlayerPrefs.SetInt(twitterKey, value);
            SaveData();
        }
    }

    public static int VIBER
    {
        get { return PlayerPrefs.GetInt(viberKey); }
        set
        {
            PlayerPrefs.SetInt(viberKey, value);
            SaveData();
        }
    }

    public static int TELEGRAM
    {
        get { return PlayerPrefs.GetInt(telegramKey); }
        set
        {
            PlayerPrefs.SetInt(telegramKey, value);
            SaveData();
        }
    }

    public static int OK
    {
        get { return PlayerPrefs.GetInt(OKKey); }
        set
        {
            PlayerPrefs.SetInt(OKKey, value);
            SaveData();
        }
    }

    public static string PHONE
    {
        get { return PlayerPrefs.GetString(phoneKey); }
        set
        {
            PlayerPrefs.SetString(phoneKey, value);
            SaveData();
        }
    }

    public static string PROMO
    {
        get { return PlayerPrefs.GetString(promoKey); }
        set
        {
            PlayerPrefs.SetString(promoKey, value);
            SaveData();
        }
    }

    public static void DeletePromo()
    {
        PlayerPrefs.DeleteKey(promoKey);
    }

    public static void DelteAllKey()
    {
        //PlayerPrefs.DeleteKey(cityKey);
        PlayerPrefs.DeleteKey(tarrifKey);
        PlayerPrefs.DeleteKey(smsKey);
        PlayerPrefs.DeleteKey(unlimitedKey);
        PlayerPrefs.DeleteKey(skypeKey);
        PlayerPrefs.DeleteKey(messageKey);
        PlayerPrefs.DeleteKey(whatsAppKey);
        PlayerPrefs.DeleteKey(facebookKey);
        PlayerPrefs.DeleteKey(vkKey);
        PlayerPrefs.DeleteKey(instagramKey);
        PlayerPrefs.DeleteKey(twitterKey);
        PlayerPrefs.DeleteKey(viberKey);
        PlayerPrefs.DeleteKey(telegramKey);
        PlayerPrefs.DeleteKey(OKKey);
        PlayerPrefs.DeleteKey(phoneKey);
        PlayerPrefs.DeleteKey(tarrifSumKey);
        PlayerPrefs.DeleteKey(tarrifMinutesKey);
        PlayerPrefs.DeleteKey(tarrifGBKey);
        PlayerPrefs.DeleteKey(tarrifTotalSumKey);
        PlayerPrefs.DeleteKey(tarrifGBPriceKey);
        PlayerPrefs.DeleteKey(social);
        PlayerPrefs.DeleteKey(tarrifMinSumm);
        SaveData();
    }

    static void SaveData()
    {
        PlayerPrefs.Save();
    }


    public class CityClass
    {
        public int MIN;
        public int GB;
        public int Price;
        public int MINSum;
        public int GBSumm;
        public int Social;
        public int Message;

        public CityClass(int min, int gb, int price)
        {
            MIN = min;
            GB = gb;
            Price = price;
        }

        public CityClass(int min, int minsumm, int gb, int gbsumm, int social, int message)
        {
            MIN = min;
            GB = gb;
            MINSum = minsumm;
            GBSumm = gbsumm;
            Social = social;
            Message = message;

        }
    }

    public static List<CityClass> MyCity
    {
        get
        {
            if (!Cities.ContainsKey(City))
            {
                City = "Москва";
            }
            List<CityClass> cities = Cities[City];
            for (int i = 0; i < cities.Count; i++)
            {
                for (int j = 0; j < cities.Count - 1; j++)
                {
                    if (cities[j].Price > cities[j + 1].Price)
                    {
                        var temp = cities[j];
                        cities[j] = cities[j + 1];
                        cities[j + 1] = temp;
                    }
                }
            }
            return cities;
        }
    }


    public static Dictionary<string, List<CityClass>> Cities = new Dictionary<string, List<CityClass>>()
    {
        {"Абакан", new List<CityClass>{
new CityClass(100,130,0,0,20,10),
new CityClass(200,150,5,100,0,0),
new CityClass(400,200,10,170,0,0),
new CityClass(800,400,15,200,0,0),
new CityClass(5000,1700,30,300,0,0),
}},
        {"Архангельск", new List<CityClass>{
new CityClass(200,250,0,0,20,10),
new CityClass(300,300,5,80,0,0),
new CityClass(600,400,10,100,0,0),
new CityClass(1500,800,15,150,0,0),
new CityClass(5000,1700,30,300,0,0),
}},
        {"Астрахань", new List<CityClass>{
new CityClass(200,200,1,50,20,10),
new CityClass(400,300,5,80,0,0),
new CityClass(800,500,10,150,0,0),
new CityClass(2000,800,15,180,0,0),
new CityClass(5000,1700,30,300,0,0),
}},
        {"Барнаул", new List<CityClass>{
new CityClass(100,130,0,0,20,10),
new CityClass(250,150,5,100,0,0),
new CityClass(400,200,10,170,0,0),
new CityClass(800,400,15,200,0,0),
new CityClass(5000,1700,30,300,0,0),
}},
        {"Белгород", new List<CityClass>{
new CityClass(200,200,0,0,20,10),
new CityClass(500,250,5,80,0,0),
new CityClass(800,300,10,100,0,0),
new CityClass(2000,800,20,200,0,0),
new CityClass(5000,1700,30,300,0,0),
}},
                {"Благовещенск",new List<CityClass>{
new CityClass(200,200,0,0,20,10),
new CityClass(500,300,5,100,0,0),
new CityClass(1000,500,10,150,0,0),
new CityClass(2000,800,15,200,0,0),
new CityClass(5000,1700,30,300,0,0),
}},
                {"Брянск", new List<CityClass>{
new CityClass(300,250,1,50,20,10),
new CityClass(600,350,5,80,0,0),
new CityClass(1000,450,10,100,0,0),
new CityClass(2000,800,15,150,0,0),
new CityClass(5000,1700,30,300,0,0),
}},
                {"Великий Новгород", new List<CityClass>{
new CityClass(200,250,0,0,20,10),
new CityClass(400,300,5,80,0,0),
new CityClass(700,400,10,100,0,0),
new CityClass(2000,800,15,150,0,0),
new CityClass(5000,1700,30,300,0,0),
}},
                {"Владивосток", new List<CityClass>{
new CityClass(200,200,0,0,20,10),
new CityClass(400,250,5,80,0,0),
new CityClass(800,400,12,100,0,0),
new CityClass(2000,800,20,150,0,0),
new CityClass(5000,1800,30,200,0,0),
}},
                {"Владикавказ", new List<CityClass>{
new CityClass(200,200,0,0,20,10),
new CityClass(500,250,5,80,0,0),
new CityClass(1000,300,10,100,0,0),
new CityClass(2000,800,20,150,0,0),
new CityClass(5000,1700,30,300,0,0),
}},
                {"Владимир", new List<CityClass>{
new CityClass(200,200,1,50,20,10),
new CityClass(400,250,5,80,0,0),
new CityClass(800,350,10,150,0,0),
new CityClass(2000,800,15,250,0,0),
new CityClass(5000,1700,30,300,0,0),
}},
                {"Волгоград", new List<CityClass>{
new CityClass(200,250,1,50,20,10),
new CityClass(500,300,5,80,0,0),
new CityClass(800,500,10,130,0,0),
new CityClass(2000,800,15,160,0,0),
new CityClass(5000,1700,30,300,0,0),
}},
                {"Вологда", new List<CityClass>{
new CityClass(250,200,0,0,20,10),
new CityClass(400,300,5,100,0,0),
new CityClass(600,400,12,200,0,0),
new CityClass(2000,800,15,250,0,0),
new CityClass(5000,1700,30,300,0,0),
}},
                {"Воронеж", new List<CityClass>{
new CityClass(200,230,0,0,20,10),
new CityClass(500,250,5,80,20,10),
new CityClass(800,300,10,100,20,10),
new CityClass(2000,800,20,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
                {"Горно-Алтайск", new List<CityClass>{
new CityClass(100,130,0,0,20,10),
new CityClass(200,150,5,100,20,10),
new CityClass(400,200,10,170,20,10),
new CityClass(800,400,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
                {"Екатеринбург", new List<CityClass>{
new CityClass(200,150,1,80,20,10),
new CityClass(400,200,5,100,20,10),
new CityClass(800,300,10,150,20,10),
new CityClass(2000,800,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
                {"Иваново", new List<CityClass>{
new CityClass(250,200,0,0,20,10),
new CityClass(500,300,5,50,20,10),
new CityClass(800,400,10,130,20,10),
new CityClass(2000,800,15,150,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
                {"Ижевск", new List<CityClass>{
new CityClass(400,200,0,0,20,10),
new CityClass(700,300,5,100,20,10),
new CityClass(1000,500,10,150,20,10),
new CityClass(2000,800,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
                {"Йошкар-Ола", new List<CityClass>{
new CityClass(200,150,1,80,20,10),
new CityClass(400,200,5,100,20,10),
new CityClass(600,300,10,150,20,10),
new CityClass(800,400,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
                {"Иркутск", new List<CityClass>{
new CityClass(200,100,0,0,20,10),
new CityClass(500,200,5,130,20,10),
new CityClass(1000,300,10,180,20,10),
new CityClass(2000,800,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Казань", new List<CityClass>{
new CityClass(200,150,1,80,20,10),
new CityClass(400,250,5,130,20,10),
new CityClass(600,350,10,180,20,10),
new CityClass(800,400,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Калининград", new List<CityClass>{
new CityClass(250,250,1,50,20,10),
new CityClass(400,300,5,80,20,10),
new CityClass(700,350,10,100,20,10),
new CityClass(2000,800,15,150,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Калуга", new List<CityClass>{
new CityClass(200,200,1,50,20,10),
new CityClass(400,250,5,80,20,10),
new CityClass(800,300,10,150,20,10),
new CityClass(2000,800,15,250,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Кемерово", new List<CityClass>{
new CityClass(100,130,0,0,20,10),
new CityClass(250,150,5,100,20,10),
new CityClass(400,200,10,170,20,10),
new CityClass(800,400,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Киров", new List<CityClass>{
new CityClass(400,200,0,0,20,10),
new CityClass(700,300,5,100,20,10),
new CityClass(1000,500,10,150,20,10),
new CityClass(2000,800,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Кострома", new List<CityClass>{
new CityClass(150,150,0,0,20,10),
new CityClass(300,200,5,80,20,10),
new CityClass(600,350,12,100,20,10),
new CityClass(2000,800,20,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Краснодар", new List<CityClass>{
new CityClass(200,200,0,0,20,10),
new CityClass(500,250,5,50,20,10),
new CityClass(800,300,10,100,20,10),
new CityClass(2000,800,20,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Красноярск", new List<CityClass>{
new CityClass(100,130,0,0,20,10),
new CityClass(200,150,5,100,20,10),
new CityClass(400,200,10,170,20,10),
new CityClass(800,400,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Курган", new List<CityClass>{
new CityClass(400,200,0,0,20,10),
new CityClass(700,300,5,100,20,10),
new CityClass(1000,500,10,150,20,10),
new CityClass(2000,800,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Курск", new List<CityClass>{
new CityClass(200,200,1,50,20,10),
new CityClass(400,250,5,80,20,10),
new CityClass(800,300,10,150,20,10),
new CityClass(2000,800,15,250,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Кызыл", new List<CityClass>{
new CityClass(200,180,0,0,20,10),
new CityClass(400,230,5,100,20,10),
new CityClass(800,400,10,170,20,10),
new CityClass(1500,700,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Липецк", new List<CityClass>{
new CityClass(200,230,0,0,20,10),
new CityClass(500,250,5,80,20,10),
new CityClass(800,300,10,100,20,10),
new CityClass(2000,800,20,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Магадан", new List<CityClass>{
new CityClass(200,300,1,50,20,10),
new CityClass(400,350,5,100,20,10),
new CityClass(700,500,10,200,20,10),
new CityClass(1500,800,15,250,20,10),
new CityClass(5000,1700,20,300,20,10),
}},
{"Магас", new List<CityClass>{
new CityClass(200,200,0,0,20,10),
new CityClass(500,250,5,100,20,10),
new CityClass(800,300,10,150,20,10),
new CityClass(2000,800,20,250,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Майкоп", new List<CityClass>{
new CityClass(200,200,0,0,20,10),
new CityClass(500,250,5,50,20,10),
new CityClass(800,300,10,100,20,10),
new CityClass(2000,800,20,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Махачкала", new List<CityClass>{
new CityClass(200,220,0,0,20,10),
new CityClass(600,270,5,80,20,10),
new CityClass(1000,350,15,100,20,10),
new CityClass(2000,800,20,150,20,10),
new CityClass(5000,1700,30,200,20,10),
}},
{"Мурманск", new List<CityClass>{
new CityClass(250,250,0,0,20,10),
new CityClass(400,300,7,100,20,10),
new CityClass(600,400,12,150,20,10),
new CityClass(2000,800,20,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Нальчик", new List<CityClass>{
new CityClass(200,200,0,0,20,10),
new CityClass(500,250,5,100,20,10),
new CityClass(1000,300,10,150,20,10),
new CityClass(2000,800,20,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Нижний Новгород", new List<CityClass>{
new CityClass(250,200,1,50,20,10),
new CityClass(400,250,5,80,20,10),
new CityClass(800,300,10,150,20,10),
new CityClass(2000,800,15,250,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Новосибирск", new List<CityClass>{
new CityClass(100,150,1,80,20,10),
new CityClass(300,200,5,100,20,10),
new CityClass(500,250,10,150,20,10),
new CityClass(1000,400,15,220,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Омск", new List<CityClass>{
new CityClass(100,130,0,0,20,10),
new CityClass(300,150,5,100,20,10),
new CityClass(500,250,10,170,20,10),
new CityClass(1000,400,20,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Орел", new List<CityClass>{
new CityClass(250,200,1,50,20,10),
new CityClass(400,250,5,80,20,10),
new CityClass(800,300,10,150,20,10),
new CityClass(2000,800,15,250,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Оренбург", new List<CityClass>{
new CityClass(200,200,1,50,20,10),
new CityClass(400,300,5,80,20,10),
new CityClass(800,500,10,150,20,10),
new CityClass(2000,800,15,180,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Пенза", new List<CityClass>{
new CityClass(200,200,1,50,20,10),
new CityClass(500,300,5,80,20,10),
new CityClass(800,500,10,130,20,10),
new CityClass(2000,800,15,160,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Пермь", new List<CityClass>{
new CityClass(400,200,1,80,20,10),
new CityClass(700,300,5,100,20,10),
new CityClass(1000,500,10,150,20,10),
new CityClass(2000,800,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Петрозаводск", new List<CityClass>{
new CityClass(200,250,0,0,20,10),
new CityClass(300,300,5,80,20,10),
new CityClass(600,400,10,100,20,10),
new CityClass(1500,800,15,150,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Псков", new List<CityClass>{
new CityClass(200,250,1,50,20,10),
new CityClass(400,300,5,80,20,10),
new CityClass(600,400,12,100,20,10),
new CityClass(1000,600,20,150,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Региональный", new List<CityClass>{
new CityClass(200,300,2,70,25,15),
new CityClass(500,350,5,100,25,15),
new CityClass(800,600,10,180,25,15),
new CityClass(2000,1000,15,250,25,15),
new CityClass(5000,2500,30,350,25,15),
}},
{"Ростов", new List<CityClass>{
new CityClass(200,200,0,0,20,10),
new CityClass(500,250,5,100,20,10),
new CityClass(1000,300,10,150,20,10),
new CityClass(2000,800,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Рязань", new List<CityClass>{
new CityClass(200,150,1,80,20,10),
new CityClass(400,250,5,100,20,10),
new CityClass(800,300,10,150,20,10),
new CityClass(2000,800,15,250,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Салехард", new List<CityClass>{
new CityClass(400,250,0,0,20,10),
new CityClass(700,300,5,100,20,10),
new CityClass(1000,500,10,150,20,10),
new CityClass(2000,800,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Самара", new List<CityClass>{
new CityClass(300,200,1,50,20,10),
new CityClass(500,300,5,80,20,10),
new CityClass(800,400,10,130,20,10),
new CityClass(2000,800,15,180,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Санкт-Петербург", new List<CityClass>{
new CityClass(200,250,0,0,20,10),
new CityClass(400,400,5,80,20,10),
new CityClass(800,500,10,100,20,10),
new CityClass(1200,800,15,150,20,10),
new CityClass(5000,2000,30,300,20,10),
}},
{"Саранск", new List<CityClass>{
new CityClass(200,200,1,50,20,10),
new CityClass(500,350,5,80,20,10),
new CityClass(800,500,10,150,20,10),
new CityClass(2000,800,15,180,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Саратов", new List<CityClass>{
new CityClass(300,230,1,80,20,10),
new CityClass(600,300,5,100,20,10),
new CityClass(800,400,10,180,20,10),
new CityClass(1000,500,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Смоленск", new List<CityClass>{
new CityClass(150,150,0,0,20,10),
new CityClass(300,200,5,80,20,10),
new CityClass(600,350,10,100,20,10),
new CityClass(2000,800,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Ставрополь", new List<CityClass>{
new CityClass(200,220,0,0,20,10),
new CityClass(500,250,5,80,20,10),
new CityClass(800,300,10,100,20,10),
new CityClass(2000,800,20,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Сыктывкар", new List<CityClass>{
new CityClass(400,200,0,0,20,10),
new CityClass(700,300,5,100,20,10),
new CityClass(1000,500,10,150,20,10),
new CityClass(2000,800,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Тамбов", new List<CityClass>{
new CityClass(200,200,0,0,20,10),
new CityClass(500,250,5,50,20,10),
new CityClass(800,300,10,100,20,10),
new CityClass(2000,800,20,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Тверь", new List<CityClass>{
new CityClass(200,230,0,0,20,10),
new CityClass(300,250,5,80,20,10),
new CityClass(500,350,10,150,20,10),
new CityClass(2000,800,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Томск", new List<CityClass>{
new CityClass(100,130,0,0,20,10),
new CityClass(300,150,5,100,20,10),
new CityClass(500,250,10,170,20,10),
new CityClass(1000,400,20,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Тула", new List<CityClass>{
new CityClass(200,200,1,50,20,10),
new CityClass(400,250,5,80,20,10),
new CityClass(800,300,10,150,20,10),
new CityClass(2000,800,15,250,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Тюмень", new List<CityClass>{
new CityClass(400,200,0,0,20,10),
new CityClass(700,300,5,100,20,10),
new CityClass(1000,500,10,150,20,10),
new CityClass(2000,800,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Улан-Удэ", new List<CityClass>{
new CityClass(300,150,1,80,20,10),
new CityClass(500,200,5,100,20,10),
new CityClass(1000,300,10,130,20,10),
new CityClass(2000,800,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Ульяновск", new List<CityClass>{
new CityClass(200,200,1,50,20,10),
new CityClass(500,300,5,80,20,10),
new CityClass(800,400,10,130,20,10),
new CityClass(2000,800,15,160,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Уфа", new List<CityClass>{
new CityClass(250,200,0,0,20,10),
new CityClass(500,300,5,80,20,10),
new CityClass(800,400,10,130,20,10),
new CityClass(2000,800,15,160,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Хабаровск", new List<CityClass>{
new CityClass(200,250,1,50,20,10),
new CityClass(500,300,5,100,20,10),
new CityClass(1000,500,10,150,20,10),
new CityClass(2000,800,20,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Ханты-Мансийск", new List<CityClass>{
new CityClass(400,250,0,0,20,10),
new CityClass(700,350,5,100,20,10),
new CityClass(1000,500,10,150,20,10),
new CityClass(2000,800,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Чебоксары", new List<CityClass>{
new CityClass(200,200,1,50,20,10),
new CityClass(500,350,5,80,20,10),
new CityClass(800,500,10,150,20,10),
new CityClass(2000,800,15,180,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Челябинск", new List<CityClass>{
new CityClass(300,200,1,80,20,10),
new CityClass(600,250,5,100,20,10),
new CityClass(1000,500,10,150,20,10),
new CityClass(2000,800,15,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Черкесск", new List<CityClass>{
new CityClass(200,200,0,0,20,10),
new CityClass(500,250,5,100,20,10),
new CityClass(800,300,10,150,20,10),
new CityClass(2000,800,20,250,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Чита",new List<CityClass>{
new CityClass(100,200,0,0,20,10),
new CityClass(400,250,5,100,20,10),
new CityClass(1000,500,15,150,20,10),
new CityClass(2000,800,20,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Элиста", new List<CityClass>{
new CityClass(200,200,1,50,20,10),
new CityClass(500,350,5,80,20,10),
new CityClass(800,500,10,150,20,10),
new CityClass(2000,800,15,180,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Южно-Сахалинск", new List<CityClass>{
new CityClass(200,250,0,0,20,10),
new CityClass(500,300,5,100,20,10),
new CityClass(1000,500,10,150,20,10),
new CityClass(2000,800,20,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Якутск", new List<CityClass>{
new CityClass(300,250,0,0,20,10),
new CityClass(0,0,5,100,20,10),
new CityClass(700,350,10,150,20,10),
new CityClass(2000,800,20,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Ярославль", new List<CityClass>{
new CityClass(300,230,0,0,20,10),
new CityClass(500,300,5,80,20,10),
new CityClass(700,350,12,150,20,10),
new CityClass(2000,800,20,250,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Биробиджан",new List<CityClass>{
new CityClass(100,200,0,0,20,10),
new CityClass(400,250,5,100,20,10),
new CityClass(600,300,10,150,20,10),
new CityClass(1000,500,20,200,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
{"Грозный", new List<CityClass>{
new CityClass(200,200,0,0,20,10),
new CityClass(500,250,5,100,20,10),
new CityClass(800,300,10,150,20,10),
new CityClass(2000,800,20,250,20,10),
new CityClass(5000,1700,30,300,20,10),
}},
        {"Москва", new List<CityClass>{
            new CityClass(200,300,2,70,25,15),
            new CityClass(500,350,5,100,0,0),
            new CityClass(800,600,10,180,0,0),
            new CityClass(2000,1000,15,250,0,0),
            new CityClass(5000,2500,30,350,0,0),
        }},
    };

}
