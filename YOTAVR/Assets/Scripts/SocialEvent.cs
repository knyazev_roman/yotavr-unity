﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SocialEvent{

	NONE = -1,
	FACEBOOK = 0,
	INSTAGRAM = 1,
	MESSAGE = 2,
	OK = 3,
	SKYPE =4,
	SMS = 5,
	TELEGRAM = 6,
	TWITTER = 7,
	VIBER = 8,
	VK = 9,
	WHATSAPP = 10
}
