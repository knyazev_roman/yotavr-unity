﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainManager : MonoBehaviour {

    public float timeLeft = 13;
    public GameObject Video;
    public GameObject Main;
    public bool IsVideoRun;
    public GameObject AudioSourceStart;

    public static bool isFirstRun = false;

    // Use this for initialization
    void Start () {

        if (UserData.IsFirstLoad == 1 || isFirstRun==false)
        {
            Video.SetActive(true);
            IsVideoRun = true;
        }
        else if (isFirstRun)
        {
            Video.SetActive(false);
            Main.SetActive(true);
        }
        UserData.IsFirstLoad = 0;
        isFirstRun = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (!IsVideoRun)
            return;
        timeLeft -= Time.deltaTime;
        if (timeLeft < 0)
        {
            Video.SetActive(false);
            Main.SetActive(true);
            AudioSourceStart.SetActive(true);
            if (!findImagesMainScene)
                FindAllImages();
        }
    }

    bool findImagesMainScene = false;
    void FindAllImages()
    {
        findImagesMainScene = true;
        //UnityEngine.UI.Image[] imgs = GameObject.FindObjectsOfType<UnityEngine.UI.Image>();
        //foreach (var item in imgs){
        //    Debug.Log(item.name);
        //}
    }
}
