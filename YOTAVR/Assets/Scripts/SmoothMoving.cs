﻿using UnityEngine;
using System.Collections;
public class SmoothMoving : MonoBehaviour
{
	private Vector3 startPosition;
	private Vector3 endPosition;
	private float speed;
	private float secondsForOneLength = 10f;

	void Start()
	{
		speed = Random.Range (0.2f, 0.8f);
		startPosition = transform.position;
		endPosition = new Vector3 (transform.position.x, transform.position.y - (Random.Range(20.0f, 60.0f)), transform.position.z);
	}

	void Update()
	{
		transform.position = Vector3.Lerp(startPosition, endPosition,
			Mathf.SmoothStep
			(
				0f,
				1f,
				Mathf.PingPong(Time.time / secondsForOneLength, speed)
			) 
		);
	}
}