﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenesData  {

    public enum SCENES
    {
        InfoScene1 = 0,
        InfoScene2 = 1,
        InfoScene3 = 2,
        GEO = 3,
        TITLE = 4,
        MAIN = 5,
        YOTA_CODE = 6,
        ADVANTAGE = 7,
        MAP = 8,
        NUF = 9,
        COMPASS = 10,
        POLZUNOK = 11,
        SMS = 12,
        TARRIF = 13,
        TOTAL = 14,
        USER_DATA = 15,
        TARRIF_DONE = 16,
        YOTA_CODE_DONE = 17,
        POLZUNOKGB = 18,
    }
}
