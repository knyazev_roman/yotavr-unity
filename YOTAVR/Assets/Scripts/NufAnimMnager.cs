﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NufAnimMnager : MonoBehaviour {

    public GameObject NextScene;
    public float Timer = 9f;

    public void NufFinish()
    {
        var anim = GetComponent<Animation>();
        anim.clip = anim.GetClip("NufFinish");
        anim.Play();
        StartCoroutine(NufFinishAudio());
    }

    public IEnumerator NufFinishAudio()
    {
        while (true)
        {
            Timer -= Time.deltaTime;
            if (Timer < 0)
                break;
            yield return new WaitForEndOfFrame();
        }
        
        print("END");
        this.gameObject.SetActive(false);
        NextScene.SetActive(true);
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
