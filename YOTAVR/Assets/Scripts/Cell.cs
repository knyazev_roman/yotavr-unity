﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cell : MonoBehaviour {
    public OpenCode openCode;
    public string CODE = "51926370";
    string url = "https://sa-wd.ru/yota";
    public const float TimerSearch = 7;
    public float Timer = TimerSearch;

    void Start()
    {
        StartCoroutine(Parse());
        //Init();
    }
    public List<GameObject> codes = new List<GameObject>();
    private void Update()
    {
        if(TimerSearch>=0 && codes.Count>0)
        {
            Timer -= Time.deltaTime;
            if(Timer<=0)
            {
                foreach (var item in codes)
                {
                    var d = item.gameObject.GetComponent<CodeItem>();
                    FindObjectOfType<OpenCode>().SetCode(d.position, d.Character);
                    Timer = TimerSearch;
                    codes.Remove(item);
                    break;
                }
            }
        }
    }

    public void Init()
    {
        List<CodeItem> CodeItems = new List<CodeItem>(FindObjectsOfType<CodeItem>());
        foreach(var item in CodeItems)
        {
            item.gameObject.SetActive(false);
        }
        int i = 0;
        foreach(var symbol in CODE)
        {
            var indexCode = openCode.CODES_STRING.IndexOf(symbol.ToString());
            var indexGO = Random.Range(0, CodeItems.Count-1);
            GameObject go = CodeItems[indexGO].gameObject;
            go.GetComponent<CodeItem>().Img = openCode.CODES_SPRITE[indexCode];
            go.GetComponent<CodeItem>().Character = openCode.CODES_STRING[indexCode];
            go.GetComponent<CodeItem>().position = i;
            go.SetActive(true);
            go.name = "cell" + i;
            codes.Add(go);
            CodeItems.RemoveAt(indexGO);
            i++;
        }
    }

    public class RootObject
    {
        public bool state { get; set; }
        public string code { get; set; }
    }

    //private readonly Random _rng = new Random();
    private const string _chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ12345678978";

    private string RandomString(int size)
    {
        char[] buffer = new char[size];

        for (int i = 0; i < size; i++)
        {
            buffer[i] = _chars[Random.Range(0,_chars.Length-1)];
        }
        return new string(buffer);
    }

    IEnumerator Parse()
    {
        WWW www = new WWW(url);
        yield return www;
        if (www.error == null)
        {
            Processjson(www.text);
        }
        else
        {
            StartCoroutine(AddLog("error parse code from server :" + www.error));
            CODE = RandomString(5);
            Init();
        }
    }

    private void Processjson(string data)
    {
        var N = SimpleJSON.JSON.Parse(data);
        var code = N["code"].Value;
        StartCoroutine(AddLog("code parse from server :"+ code));
        CODE = code;
        Init();
    }

    IEnumerator AddLog(string text)
    {
        WWW www = new WWW("https://sa-wd.ru/yota/addlog?session=" + WWW.EscapeURL(SessionLog.session_code) + "&&data=" + WWW.EscapeURL(text));
        yield return www;
        if (www.error == null)
        {

        }
    }
}
