﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableYotaCode : MonoBehaviour
{
    public GameObject connection;
    public GameObject yotaCode;


    void Start()
    {
        if (UserData.yotaCodePassCities.IndexOf(UserData.City) < 0)
        {
            if (yotaCode != null)
                yotaCode.SetActive(false);

            if (connection != null)
                connection.SetActive(true);
        }
    }
}
