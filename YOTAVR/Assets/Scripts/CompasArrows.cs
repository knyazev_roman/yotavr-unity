﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CompasArrows : MonoBehaviour {

    public bool IsReadyRotate;
    public bool IsRunCompasRotate;
    public Animation AnimClip;
    public GvrHead Head;
    public float Angle = 0.15f;
    public GameObject compas;
    private float z = 0, z2 = 0;
    private float eps = 5;
    private float rotateSpeed = 1.6f;


    public void Ready(int done)
    {
        IsReadyRotate = true;
    }

    // Use this for initialization
    void Start () {
        if (Head == null)
            Head = FindObjectOfType<GvrHead>();
        AnimClip["CompasRotate"].speed = 0;
        AnimClip.Play("CompasRotate");
    }

    void Update()
    {
        if (!IsRunCompasRotate)
        {
            // Вращаем кнопкой
            if (Input.GetButton("Tap"))
            {
                compas.GetComponent<Animation>().enabled = false;
                z -= Head.transform.rotation.eulerAngles.y;
                compas.transform.Rotate(0, 0, z * 4f);
            }

            // Вращаем головой
            //z2 -= Head.transform.rotation.eulerAngles.z;
            //compas.transform.Rotate(0, 0, z2);
            //z2 = Head.transform.rotation.eulerAngles.z;
        }

        z = Head.transform.rotation.eulerAngles.y;

        /*
        if (Mathf.Abs(Head.transform.rotation.z) > Angle)
        {
            if (IsReadyRotate && !IsRunCompasRotate)
            {
                AnimClip["CompasRotate"].speed = 1;
                AnimClip.enabled = true;
                IsRunCompasRotate = true;
            }
        }
         * */

        if (compas.transform.rotation.eulerAngles.z >= -eps && compas.transform.rotation.eulerAngles.z <= eps)
        {
            if (!IsRunCompasRotate)
            {

#if UNITY_EDITOR
                Cursor.lockState = CursorLockMode.None;
#endif

                //compas.GetComponent<Animation>().enabled = true;
                AnimClip["CompasRotate"].speed = 1;
                AnimClip.enabled = true;
                IsRunCompasRotate = true;
            }
        }
    }
}