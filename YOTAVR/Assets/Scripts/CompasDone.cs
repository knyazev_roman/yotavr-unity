﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompasDone : MonoBehaviour {

    public GameObject DoneObject;
    public bool IsDone = false;
    GameObject[] objs;
    public float Timer = 3f;
    public Animation AnimClip;
    public GameObject doneGameObj;
    public GameObject compas;
    public GameObject hideDoneText;

    public void Done(int done)
    {
        objs = GameObject.FindGameObjectsWithTag("Pieces");
        print(objs.Length);
        
        var audios = FindObjectsOfType<AudioSource>() as AudioSource[];
        foreach (AudioSource item in audios)
        {
            item.Stop();
        }

        DoneObject.SetActive(true);
        print("Done");
        //CallDestroy();
        AnimClip.clip = AnimClip.GetClip("CompassFinish");
        AnimClip.Play();
        StartCoroutine(DoneFinishAudio());
        IsDone = true;
    }

    public IEnumerator DoneFinishAudio()
    {
        while (true)
        {
            Timer -= Time.deltaTime;
            if (Timer < 0)
                break;
            yield return new WaitForEndOfFrame();
        }
        CallDestroy();
        print("END");
    }

    bool _oCall = false;
    void CallDestroy()
    {
        if (_oCall) return;
        _oCall = true;
        foreach (GameObject item in objs)
        {
            foreach (Transform MeshItem in item.transform)
            {
                if (MeshItem.GetComponent<MeshRenderer>() != null)
                {
                    MeshItem.gameObject.AddComponent<BoxCollider>();
                    MeshItem.gameObject.AddComponent<Rigidbody>();
                    MeshItem.gameObject.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * 675 * -1, ForceMode.Acceleration);
                    MeshItem.gameObject.AddComponent<LowTrans>();
                    if (MeshItem.gameObject.GetComponent<RandomCrystal>() != null)
                    {
                        MeshItem.gameObject.GetComponent<RandomCrystal>().enabled = false;
                    }
                }
            }
        }
        doneGameObj.SetActive(true);
        hideDoneText.SetActive(false);
    }

    // Use this for initialization
    void Start () {
		
	}
	
}
