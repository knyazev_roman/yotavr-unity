﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlimitedAnimMnager : MonoBehaviour {

    public GameObject NextScene;
    public float Timer = 7f;

    public void UnlimitedFinish()
    {
        var anim = GetComponent<Animation>();
        anim.clip = anim.GetClip("UnlimitedFinish");
        anim.Play();
        StartCoroutine(UnlimitedFinishAudio());
    }

    public IEnumerator UnlimitedFinishAudio()
    {
        while (true)
        {
            Timer -= Time.deltaTime;
            if (Timer < 0)
                break;
            yield return new WaitForEndOfFrame();
        }

        
        this.gameObject.SetActive(false);
        NextScene.SetActive(true);
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
