﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GeolocationYota : MonoBehaviour {

    public Text TextField;

    // Wait until service initializes
    public int maxWait = 10;

    IEnumerator Start()
    {
        // Start service before querying location
        Input.location.Start();
        while (true)
        {
            yield return new WaitForSeconds(1);
            if(Input.location.status == LocationServiceStatus.Running)
            {
                var t = "Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp;
                TextField.text = t;
                // Access granted and location value could be retrieved
                print(t);
                TextField.text = "DONE-2";
                FindObjectOfType<GetCityYota>().GetCityName(Input.location.lastData.latitude, Input.location.lastData.longitude);
                // Stop service if there is no need to query location updates continuously
                Input.location.Stop();
                break;
            }
        }  
    }

    IEnumerator Start2()
    {

        TextField.text = "RUN!";
        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
        {
            yield break; 
        }

        // Start service before querying location
        Input.location.Start();

        
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            print("Timed out");
            TextField.text = "Timed out";
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            print("Unable to determine device location");
            TextField.text = "Unable to determine device location";
            yield break;
        }
        else
        {
            var t = "Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp;
            TextField.text = t;
            // Access granted and location value could be retrieved
            print(t);
            TextField.text = "DONE-2";
            FindObjectOfType<GetCityYota>().GetCityName(Input.location.lastData.latitude, Input.location.lastData.longitude);
        }

        // Stop service if there is no need to query location updates continuously
        Input.location.Stop();
    }

}
