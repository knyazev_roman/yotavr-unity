﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CodeItem : MonoBehaviour {

    public int position;

    private Sprite img;

    public Sprite Img
    {
        get { return img; }
        set
        {
            img = value;
            GetComponentInChildren<SpriteRenderer>().sprite = value;
        }
    }
    public string Character;

    private float fillTime = 1f;
    private float lerpTime = .5f;
    private float timer = 0;

    public bool found = false;      // Зацепили символ камерой
    private bool filled = false;    // Заполнен ли символ
    private bool lerp = false;      // Доставлен ли символ
    public bool active = true;     // Заполено и доставлено; отключаем управление
    public AudioSource source;
    public AudioClip clipSet;
    public GameObject target;



    Vector3 a;

    void Start () {
        //Camera camera = Camera.main;            
        //transform.LookAt(transform.position + camera.transform.rotation * Vector3.forward, camera.transform.rotation * Vector3.up);

        a = transform.position;
    }

    void Update()
    {
        if (found && active)
        {
            timer += Time.deltaTime;
            if (!filled)
            {
                SetAlpha(timer / fillTime);
                if (timer >= fillTime)
                {
                    timer = 0;
                    filled = true;
                }
            }
            if (filled)
            {
                SetAlpha(1 - timer / lerpTime);
                transform.position = Vector3.Lerp(a, target.transform.position, timer / lerpTime);
                if (timer >= lerpTime)
                {
                    timer = 0;
                    lerp = true;
                }
            }
            if (lerp)
            {
                source.PlayOneShot(clipSet);
                active = false;
                FindObjectOfType<Cell>().codes.Remove(this.gameObject);
                FindObjectOfType<OpenCode>().SetCode(position, Character);
            }
        }
    }

    void ResetItem()
    {
        FillItem();
    }

    void FillItem()
    {
        while (timer < fillTime)
        {
            timer += Time.deltaTime;
            SetAlpha(1 - timer / fillTime);
            if (timer >= fillTime)
            {
                FindObjectOfType<LookCode>().ResetForItem();
                timer = 0;
                break;
            }
        }
    }

    public void SetAlpha(float value)
    {
        var img = GetComponent<SpriteRenderer>();
        var color = img.color;
        color.a = value;
        img.color = color;
    }
}
