﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchDraw : MonoBehaviour {

    public float timerTouch = 0.5f;
    public bool NeedTouch = false;
    public bool CanDraw = true;
    public bool OneTouchOnly = true;

    public AudioSource Source;
    public AudioClip StartSound;
    public AudioClip PressButton;
    public AudioClip ExitDraw;
    // Use this for initialization
    void Start () {
        if (!NeedTouch){
            CanDraw = true;
        }

        Source = this.gameObject.AddComponent<AudioSource>();
        StartSound = Resources.Load("yota_BTTN_start") as AudioClip;

        Source.PlayOneShot(StartSound);

        PressButton = Resources.Load("yota_BTTN_press") as AudioClip;
        ExitDraw = Resources.Load("yota_BTTN_end") as AudioClip;


        //yota_BTTN_end
    }

    // Update is called once per frame
    void Update () {
        if (!NeedTouch)
            return;

        timerTouch += Time.deltaTime;
#if UNITY_EDITOR
        if (Input.GetMouseButton(0) && timerTouch > 1){
            CanDraw = !CanDraw;

            //print("draw: "+ CanDraw);
            timerTouch = 0;

            if (CanDraw)
                Source.PlayOneShot(PressButton);
            else
                Source.PlayOneShot(ExitDraw);

            return;
        }
#endif
        if (Input.GetMouseButtonDown(0))
        {
            CanDraw = !CanDraw;

            timerTouch = 0;

            if (CanDraw)
                Source.PlayOneShot(PressButton);
            else
                Source.PlayOneShot(ExitDraw);
        }
        /*if (Input.touchCount > 0 && timerTouch > 1)
        {
            CanDraw = !CanDraw;
            timerTouch = 0;
        }*/
    }
}
