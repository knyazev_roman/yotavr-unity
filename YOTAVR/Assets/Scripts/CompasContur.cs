﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CompasContur : MonoBehaviour {

    public bool IsReady;
    public bool IsRunContur;
    public Animation AnimClip;

    public void Ready(int done)
    {
        IsReady = true;
    }



    // Use this for initialization
    void Start () {
        
        AnimClip["CompasContur"].speed = 0;
        //AnimClip.Play("CompasContur");
    }

    IEnumerator CheckLocation()
    {
        while (true)
        {
            var t = Input.compass.magneticHeading;
            yield return new WaitForSeconds(0.05f);
        }
    }

    void Update()
    {
        if (IsReady && !IsRunContur)
        {
            AnimClip["CompasContur"].speed = 4;
            AnimClip.Play("CompasContur");
            IsRunContur = true;
        }
    }
}