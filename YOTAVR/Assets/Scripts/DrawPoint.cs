﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;

public class DrawPoint : MonoBehaviour
{

    public int NeedDrawed;
    public Text CountDraw;
    public ScenesData.SCENES Scene;
    public LayerMask mask;
    public bool draw;
    public bool IsDone;
    public bool FastLoad = true;
    public List<GameObject> NeededDraw;
    public DoneType TypeDone;
    public bool UseDestroy = false;
    public List<BoxCollider> objs;
    public bool CustumLogicisDone = false;
    public SpriteRenderer[] UiHides;
    public float force = 675;

    public StepObject step;

    public GameObject anotherScenes;


    public GameObject SoundParticleEffectFinish;

    public enum DoneType
    {
        COUNT = 0,
        SHOWING = 1,
    }

    public AudioSource sourceAudio;
    public AudioClip[] Sounds;

    void Start()
    {
        anotherScenes.SetActive(false);
        anotherScenes.SetActive(true);

        FindObjectOfType<TouchDraw>().CanDraw = false;

        objs = new List<BoxCollider>();
        var tempObjs = FindObjectsOfType<BoxCollider>();

        //if (SelfDraws==null)
        //    SelfDraws = new List<SelfDraw>();

        int c = 0;
        foreach (var item in tempObjs)
        {
            if (item.enabled)
            {
                //.GetChild(0).gameObject.
                //var d = item.gameObject.transform.GetChild(0).gameObject.AddComponent<SelfDraw>();
                ////SelfDraws.Add(d);
                //d.Index = c + 1;
                objs.Add(item);
                c++;
            }
        }


        if (Scene == ScenesData.SCENES.MAP || Scene == ScenesData.SCENES.NUF)
        {
            foreach (GameObject item in NeededDraw)
            {
                item.transform.GetChild(0).gameObject.AddComponent<CrystalBaseItem>();
            }
        }
        //if(Scene == ScenesData.SCENES.COMPASS && NeededDraw.Count==0)
        //      {
        //          //NeededDraw = new List<GameObject>();
        //          foreach (var item in GameObject.FindObjectsOfType<NufItem>())
        //          {
        //              NeededDraw.Add(item.gameObject);
        //          }
        //          //NeededDraw = GameObject.FindObjectsOfType<NufItem>().ToList<GameObject>();
        //      }
    }

    bool _oCall = false;
    public void CallDestroy()
    {
        if (_oCall) return;
        _oCall = true;
        print(objs.Count);
        foreach (BoxCollider item in objs)
        {
            print("DD");
            item.transform.GetChild(0).gameObject.SetActive(true);
            item.transform.GetChild(0).transform.parent.GetComponent<BoxCollider>().enabled = false;
            foreach (Transform MeshItem in item.transform.GetChild(0).gameObject.transform)
            {
                if (MeshItem.GetComponent<MeshRenderer>() != null)
                {
                    MeshItem.gameObject.AddComponent<BoxCollider>();
                    MeshItem.gameObject.AddComponent<Rigidbody>();
                    MeshItem.gameObject.GetComponent<Rigidbody>().AddForce(-Camera.main.transform.forward * force, ForceMode.Acceleration);
                    MeshItem.gameObject.AddComponent<LowTrans>();
                    if (MeshItem.gameObject.GetComponent<RandomCrystal>() != null)
                    {
                        MeshItem.gameObject.GetComponent<RandomCrystal>().enabled = false;
                    }
                }
            }
            if (SoundParticleEffectFinish != null)
                SoundParticleEffectFinish.gameObject.SetActive(true);
        }
    }

    IEnumerator LoadScene()
    {
        print("load");
        float fadeTime = FindObjectOfType<Fading>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime);
        FindObjectOfType<TimerLoadScene>().Wait = false;
        //SceneManager.LoadScene((int)Scene);
    }

    public List<SelfDraw> SelfDraws;
    public int DrawIndex;
    private IEnumerator DrawSelf()
    {
        while (!IsDone)
        {
            if (DrawIndex >= SelfDraws.Count)
            {
                DrawIndex = 0;
            }
            else
            {
                DrawIndex++;
            }

            //print("DI:" + DrawIndex + " COUNT:" + SelfDraws.Count);
            foreach (var item in SelfDraws)
            {
                if (item.Index == DrawIndex && !item.gameObject.activeSelf)
                {
                    item.gameObject.SetActive(true);
                    item.transform.gameObject.layer = 0;
                    item.transform.GetChild(0).gameObject.layer = 0;
                    item.transform.transform.GetChild(0).gameObject.SetActive(true);

                    yield return new WaitForSeconds(0.3f);
                }
            }

            IsDone = CheckDone();
            //print(IsDone);
        }
    }


    bool CheckDone()
    {
        foreach (var item in NeededDraw)
        {
            if (item.layer != 0)
                return false;
        }
        return true;
    }

    public bool IsStartDraw;
    private float TimerLeft;
    public float Timer = 2f;
    // Update is called once per frame
    void Update()
    {
        //if (IsDone && FastLoad)
        //{
        //    StartCoroutine(LoadScene());
        //}
        //if (!FindObjectOfType<TouchDraw>().CanDraw) return;

        if (Input.GetButton("Tap"))
            draw = true;
        else
            draw = false;

        if (IsDone)
        {
            this.enabled = false;
            FindObjectOfType<NufAnimMnager>().NufFinish();
        }
        /*
        if (IsDone && FastLoad && !CustumLogicisDone)
        {
            //SceneManager.LoadScene((int)Scene);
            StartCoroutine(LoadScene());
        }
        */
        /*if (UseDestroy && IsDone)
        {
            CallDestroy();
            if (IsDone && !FastLoad && CustumLogicisDone)
            {
                step.IsDone = true;
                return;
            }
        }*/
        /*if (IsDone && FastLoad)
        {
            SceneManager.LoadScene((int)Scene);
        } */

        if (IsStartDraw)
        {
            TimerLeft -= Time.deltaTime;

            if (TimerLeft <= 0)
            {
                draw = false;
                TimerLeft = Timer;
                StartCoroutine(DrawSelf());
            }
        }

        if (!draw)
            return;
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000f, mask))
        {
            if (hit.collider.CompareTag("Exit"))
                return;
            //print(hit.transform.gameObject.name);
            TimerLeft = Timer;
            hit.collider.transform.gameObject.layer = 0;
            IsStartDraw = true;
            hit.collider.transform.GetChild(0).gameObject.layer = 0;
            hit.collider.transform.GetChild(0).gameObject.SetActive(true);
            sourceAudio.PlayOneShot(Sounds[UnityEngine.Random.Range(0, Sounds.Length)]);
            NeedDrawed--;
        }

        int t = 0;
        foreach (var item in NeededDraw)
        {
            if (item.layer != 0)
                t++;
        }

        if (TypeDone == DoneType.COUNT)
        {
            if (NeedDrawed <= 0)
            {
                IsDone = true;
                return;
            }
            else
                return;
        }
        else
        {
            foreach (var item in NeededDraw)
            {
                if (item.layer != 0)
                    return;
            }
        }
        IsDone = true;
    }


}
