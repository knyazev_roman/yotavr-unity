﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlimitedMonitor : MonoBehaviour {

    DrawUnlimited drawUnlimited;
    public UnityEngine.UI.Image Text;
    public float timeLeft = 5f;
    bool showIsDone = false;
    public GameObject Done;
    public AudioSource sourceAudio;

    // Use this for initialization
    void Start () {
        drawUnlimited = this.GetComponent<DrawUnlimited>();
        Text.gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        if (!showIsDone)
        {
            if (drawUnlimited.IsDone)
            {
                sourceAudio.Stop();
                showIsDone = true;
                Text.gameObject.SetActive(true);
                Done.SetActive(true);
            }
        }
        else
        {
            timeLeft -= Time.deltaTime;
            if (timeLeft < 0)
            {
                //drawUnlimited.FastLoad = true;
                FindObjectOfType<DrawUnlimited>().CallDestroy();
                print("END");
            }
        }
    }
}
