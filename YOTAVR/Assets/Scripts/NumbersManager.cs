﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumbersManager : MonoBehaviour {

    public int CountDigits = 3;
    public List<PriceItem> ItemPrice;

    public void SetDone(int value)
    {
        
        if(CountDigits == value)
        {
            GetComponent<Animator>().SetInteger("State", (int)ShowPrice.AnimState.Idle);
        }
            
    }

    public void HideItem(int index)
    {
        Vector3 vector = transform.GetComponent<RectTransform>().anchoredPosition3D;
        if (CountDigits == 1) vector.x = 200;
        else if (CountDigits == 2) vector.x = 150;
        //else if (CountDigits == 3) vector.x = 100;
        //else if (CountDigits == 4) vector.x = 50;

        transform.GetComponent<RectTransform>().anchoredPosition3D = vector;
        ItemPrice[index].Hide();
    }

    public void RotateItem(int index)
    {
        ItemPrice[index].Hide();
        if (index < 3)
        {
            if (ItemPrice[index + 1].Character == null)
            {
                for (int i = index + 1; i < 4; i++)
                {
                    ItemPrice[i].Hide();
                }
                GetComponent<Animator>().SetInteger("State", (int)ShowPrice.AnimState.Idle);
            }
        }
        if (!ItemPrice[index].Show())
        {
            GetComponent<Animator>().SetInteger("State", (int)ShowPrice.AnimState.Idle);
        }
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
