﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ConfirmCity : MonoBehaviour {

    public Text Title;
    public ScenesData.SCENES Scene;

    public void Confirm()
    {
        UserData.City = Title.text;
        Input.location.Stop();
        SceneManager.LoadScene((int)Scene);
        print(UserData.City);
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
