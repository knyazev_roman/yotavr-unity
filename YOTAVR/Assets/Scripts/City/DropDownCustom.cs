﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropDownCustom : MonoBehaviour {

    public Text Title;
    public GameObject Cities;
    public int SelectedIndex;
    public bool IsInit = true;
    public GameObject Parent;

    public void Switch()
    {
        Cities.SetActive(!Cities.activeSelf);
        Parent.SetActive(!Cities.activeSelf);
        IsInit = false;
    }

    public void Select(int index)
    {
        //Title.text = Cities.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(index).GetComponentInChildren<Text>().text;
        Title.text = LoadCityList.cities[index];
        if(!IsInit)
            Switch();
    }

    // Use this for initialization
    void Start () {
        Select(0);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
