﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchEventCustom : MonoBehaviour {

    public GameObject PopUp;
    public GameObject CityButton;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
#if UNITY_EDITOR
        if (Input.GetMouseButton(0))
        {
            //checkTouch(Input.mousePosition);

            HidePopup();
        }
#endif

        if (Input.touchCount > 0)
        {
            HidePopup();
        }
    }

    void checkTouch(Vector3 pos)
    {


        return;
        print(pos);
        Vector3 wp = Camera.main.ScreenToWorldPoint(pos);
        Vector2 touchPos = new Vector2(wp.x, wp.y);
        
        Collider2D hit = Physics2D.OverlapPoint(touchPos);

        if (hit)
        {
            print(hit.name);
            //hit.transform.gameObject.SendMessage("Clicked", 0, SendMessageOptions.DontRequireReceiver);
        }
    }

    public void HidePopup()
    {
        PopUp.SetActive(false);
        CityButton.SetActive(true);
    }
}
