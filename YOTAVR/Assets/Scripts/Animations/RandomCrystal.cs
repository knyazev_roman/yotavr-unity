﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomCrystal : MonoBehaviour {
    public Vector3 StartScale;
    public Vector3 StartPosition;
    public Quaternion qtStart;
    public float step = 200;
    public float rangeDist = 10;

    public bool ForceStart = false;

    private void Start()
    {
        if(ForceStart)
        {
            this.StartScale = this.transform.localScale;
            this.qtStart = this.transform.rotation;
            this.StartScale = Vector3.zero;
        }
        //Debug.Log(this.name);
        this.StartScale = this.transform.localScale;
        StartPosition = this.transform.position;
        //this.step = 2;
        //this.step = Random.Range(56, 100);
        //this.step = 200;
        this.transform.localScale = Vector3.zero;
        this.qtStart = transform.rotation;
        this.transform.rotation = Quaternion.identity;
        this.transform.position = this.transform.position + new Vector3(Random.Range(-rangeDist, rangeDist), Random.Range(-rangeDist, rangeDist), Random.Range(-rangeDist, rangeDist));
    }

    private void Update()
    {
        //this.transform.localScale += new Vector3(step * Time.deltaTime, step * Time.deltaTime, step * Time.deltaTime);
        //this.transform.Rotate(0, 1, 0);
        if (this.transform.localScale.x < StartScale.x)
            this.transform.localScale += new Vector3(step * Time.deltaTime, 0, 0);
        if (this.transform.localScale.y < StartScale.y)
            this.transform.localScale += new Vector3(0, step * Time.deltaTime, 0);
        if (this.transform.localScale.z < StartScale.z)
            this.transform.localScale += new Vector3(0, 0, step * Time.deltaTime);


        this.transform.rotation = Quaternion.RotateTowards(transform.rotation, qtStart, (Time.deltaTime * step*4));
        //float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, StartPosition, step);

        if (this.transform.localScale.z >= StartScale.z)
            if (this.transform.localScale.y >= StartScale.y)
                if (this.transform.localScale.x >= StartScale.x)
                    this.enabled = false;
    }
}
