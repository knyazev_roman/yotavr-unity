﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LowTrans : MonoBehaviour {

    float Delay = 1;
    Color colorBegin = Color.blue;
    Color colorEnd = Color.white;

    public int CountCollision = 0;
    public bool isDestroy=false;
    void OnCollisionEnter(Collision collision)
    {
        isDestroy = true;
        CountCollision++;
        if(CountCollision>1)
        {
            this.GetComponent<BoxCollider>().isTrigger = true;
            StartCoroutine(CallW());
        }

        if(collision.gameObject.tag=="plane")
        {
            this.GetComponent<BoxCollider>().isTrigger = false;
        }

        Destroy(this.gameObject, 10);
    }

    IEnumerator CallW()
    {
        yield return new WaitForSeconds(0.3f);
        this.GetComponent<BoxCollider>().isTrigger = false;
    }

    float t = 1;
    void Update()
    {
        if (isDestroy)
        {
            t = t - (0.1f * Time.deltaTime);
            this.GetComponent<MeshRenderer>().material.color = new Color(
                this.GetComponent<MeshRenderer>().material.color.r,
                this.GetComponent<MeshRenderer>().material.color.g,
                this.GetComponent<MeshRenderer>().material.color.b,
                t
                );

        }

        if(this.transform.position.y<=-50)
        {
            Destroy(this.gameObject);
        }
    }
}
