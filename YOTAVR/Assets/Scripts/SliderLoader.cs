﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderLoader : MonoBehaviour {

    public LayerMask mask;
    public GameObject lookat;

    [SerializeField]
    private Slider m_Slider;
    public float fillTime = 1;
    float timer;
    private bool lookatUI;
    private GameObject PrevObject;

    public AudioSource Source;
    public AudioClip clipFocus;

    // Use this for initialization
    void Start () {
        Source = this.gameObject.AddComponent<AudioSource>();
        clipFocus = Resources.Load("yota_FX_focus_set") as AudioClip;

        AudioSource[] performAudisoSettings = GameObject.FindObjectsOfType<AudioSource>();
        foreach (var item in performAudisoSettings)
        {
            item.minDistance = 200;
            item.maxDistance = 1000;
        }
        //clipFocus = (AudioClip)Resources.Load("Sounds/yota_FX_focus_set", typeof(AudioClip)); ;//(AudioClip)Resources.Load("Sounds/yota_FX_focus_set");
    }

    private void FillBar()
    {
        if (timer < fillTime)
        {
            if (lookatUI)
            {
                timer += Time.deltaTime;
                SetSliderValue(timer / fillTime);
            }
        }
    }

    void reset(GameObject hit)
    {
        SetSliderValue(0f);
        timer = 0;

    }

    private void SetSliderValue(float sliderValue)
    {
        // If there is a slider component set it's value to the given slider value.
        if (m_Slider)
            m_Slider.value = sliderValue;
    }

    // Update is called once per frame
    void Update () {
       
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 2000f, mask))
        {
            if (PrevObject != null && PrevObject != hit.collider.gameObject)
            {
                lookatUI = false;
                reset(hit.transform.gameObject);
                if (hit.transform.gameObject.GetComponent<SliderDoneEvent>() != null)
                    Source.PlayOneShot(clipFocus);
            }
            PrevObject = hit.collider.gameObject;
            //print(hit.collider.name);
            lookatUI = true;

        }
        else
        {
            lookatUI = false;
            reset(null);
            //PrevObject = null;
        }
        if (hit.transform != null && hit.transform.gameObject == lookat && lookat)
            FillBar();
    }
}
