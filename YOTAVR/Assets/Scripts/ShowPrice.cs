﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowPrice : MonoBehaviour {

    public Slider slider;
    public List<PriceItem> ItemPrice;
    public Text GB;
    public Text Minutes;
    CustomSliderValue sliderValue;

    // Use this for initialization
    void Start () {
        slider.value = UserData.Tarrif;
	}
    public bool isSetPrice = false;
    bool isRotarate = false;
    int indexRotate = 0;
    int CharRotrate = 3;
    public Animator animator;
    public float Timer = 0.1f;
    public float TimerLeft;

    public const int speedRotateChar = 5;

    public bool IsMinutes = true;

    public enum AnimState
    {
        Idle = 0,
        Rotate = 1,
        Exit = 2,
    }

    public void SetPrice(CustomSliderValue sliderValue)
    {
        if (IsMinutes) sliderValue.Value = sliderValue.Minutes;
        if (!IsMinutes) sliderValue.Value = sliderValue.GB;
        isSetPrice = true;
        animator.SetInteger("State", (int)AnimState.Idle);
        //if (sliderValue.Value < 1000)
        //{
        //    ItemPrice[3].Hide();
        //}
        FindObjectOfType<NumbersManager>().CountDigits = sliderValue.Value.ToString().Length;
        int i = 0;
        string t = "";
        foreach (var itemPriceq in ItemPrice)
        {
            itemPriceq.Character = null;

            if (i <= sliderValue.Value.ToString().Length - 1)
            {
                var character = sliderValue.Value.ToString()[i];
                //itemPriceq.CharacterTemp = character.ToString();
                itemPriceq.Character = character.ToString();
                //itemPriceq.Show();
                t += character;
            }
            i++;
        }
        
        //print(Time.deltaTime+" - "+t);
        //animator.SetInteger("State", (int)AnimState.Idle);
        this.sliderValue = sliderValue;
        isSetPrice = true;
        return;
        
        
        isRotarate = true;
        indexRotate = 0;
        CharRotrate = slider.value.ToString().Length;
        //print(sliderValue.Value);
    }
    private void Update()
    {
        if (!isSetPrice)
            return;
        TimerLeft -= Time.deltaTime;
        if (TimerLeft > 0)
            return;
        animator.SetInteger("State", (int)AnimState.Rotate);
        if (sliderValue != null)
        {
            GB.text = sliderValue.GB.ToString().ToUpper();
            if (IsMinutes) Minutes.text = (sliderValue.MINSum).ToString().ToUpper();//sliderValue.Minutes.ToString().ToUpper();
            if (!IsMinutes) Minutes.text = (sliderValue.GBSumm + UserData.Social).ToString().ToUpper();
            TimerLeft = Timer;
            isSetPrice = false;
        }
        //75
        //85
        //100
        //115
        return;
        if(isSetPrice && isRotarate)
        {
            if(RotarateHide())
            {
                isSetPrice = false;
            }
        }
        else if(isRotarate)
        {
            isSetPrice = false;
            int i = 0;
            GB.text = sliderValue.GB.ToString();
            Minutes.text = sliderValue.Minutes.ToString();
            foreach (var itemPriceq in ItemPrice)
            {
                itemPriceq.Hide();
                if (i <= sliderValue.Value.ToString().Length - 1)
                {
                    var character = sliderValue.Value.ToString()[i];
                    itemPriceq.Character = character.ToString();
                }
                i++;
            }
            isRotarate = false;
        }
        else if(isRotarate == false&& isSetPrice==false)
        {
            RotarateShow();
        }
    }

    bool RotarateHide()
    {
        if (ItemPrice[indexRotate].gameObject.transform.localRotation.eulerAngles.y <= 75)
        {
            ItemPrice[indexRotate].gameObject.transform.Rotate(0, speedRotateChar, 0);
        }
        else
        {
            if (ItemPrice[indexRotate+1].gameObject.transform.localRotation.eulerAngles.y <= 85)
            {
                ItemPrice[indexRotate+1].gameObject.transform.Rotate(0, speedRotateChar, 0);
            }
            else
            {
                if (ItemPrice[indexRotate + 2].gameObject.transform.localRotation.eulerAngles.y <= 100)
                {
                    ItemPrice[indexRotate + 2].gameObject.transform.Rotate(0, speedRotateChar, 0);
                }
                else
                {
                    if (ItemPrice[indexRotate + 3].gameObject.transform.localRotation.eulerAngles.y <= 100)
                    {
                        ItemPrice[indexRotate + 3].gameObject.transform.Rotate(0, speedRotateChar, 0);
                    }
                    else
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    bool RotarateShow()
    {
        foreach (var item in ItemPrice)
        {
            if(item.gameObject.transform.localRotation.eulerAngles.y > 10)
            {
                item.gameObject.transform.Rotate(0, -speedRotateChar, 0);
            }
            Debug.Log(item.gameObject.transform.localRotation.eulerAngles.y);
        }
        return false;
        //if (ItemPrice[indexRotate + 3].gameObject.transform.localRotation.eulerAngles.y > -1)
        //{
        //    ItemPrice[indexRotate + 2].gameObject.transform.Rotate(0, -speedRotateChar, 0);
        //}
        //else
        //{
        //    if (ItemPrice[indexRotate + 2].gameObject.transform.localRotation.eulerAngles.y >= 0)
        //    {
        //        ItemPrice[indexRotate + 2].gameObject.transform.Rotate(0, -speedRotateChar, 0);
        //    }
        //    else
        //    {
        //        if (ItemPrice[indexRotate + 1].gameObject.transform.localRotation.eulerAngles.y >= 0)
        //        {
        //            ItemPrice[indexRotate + 1].gameObject.transform.Rotate(0, -speedRotateChar, 0);
        //        }
        //        else
        //        {
        //            if (ItemPrice[indexRotate].gameObject.transform.localRotation.eulerAngles.y >= 0)
        //            {
        //                ItemPrice[indexRotate].gameObject.transform.Rotate(0, -speedRotateChar, 0);
        //            }
        //            else
        //            {
        //                return true;
        //            }
        //        }
        //    }
        //}
        //return false;
    }

    [Obsolete("Выпилить потом")]
    private void Update123()
    {
        if (!isRotarate) return;
        if (isSetPrice)
        {
            foreach (var item in ItemPrice)
            {
                if (item.gameObject.transform.localRotation.eulerAngles.y<=90)
                {
                    item.gameObject.transform.Rotate(0, 1, 0);
                }
                else
                {
                    int i = 0;
                    GB.text = sliderValue.GB.ToString();
                    Minutes.text = sliderValue.Minutes.ToString();
                    foreach (var itemPriceq in ItemPrice)
                    {
                        itemPriceq.Hide();
                        if(i<= sliderValue.Value.ToString().Length-1)
                        {
                            var character = sliderValue.Value.ToString()[i];
                            itemPriceq.Character = character.ToString();
                        }
                        i++;
                    }
                    isSetPrice = false;
                }
            }
        }
        else
        {
            //return;
            var item = ItemPrice[indexRotate];
            if (item.gameObject.transform.localRotation.eulerAngles.y > 0)
            {
                item.gameObject.transform.Rotate(0, -1, 0);
                if (item.gameObject.transform.localRotation.eulerAngles.y > 300)
                {
                    item.gameObject.transform.localRotation = Quaternion.identity;
                    indexRotate++;
                }
            }
            isRotarate = indexRotate != ItemPrice.Count;
            /*foreach (var item in ItemPrice)
            {
                if (item.gameObject.transform.localRotation.eulerAngles.y > 0)
                {
                    item.gameObject.transform.Rotate(0, -1, 0);
                    if(item.gameObject.transform.localRotation.eulerAngles.y>300)
                    {
                        isRotarate = false;
                        item.gameObject.transform.localRotation = Quaternion.identity;
                    }
                }
            }*/
        }
    }
}
