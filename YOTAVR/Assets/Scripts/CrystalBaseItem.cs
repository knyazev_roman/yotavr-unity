﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CrystalBaseItem : MonoBehaviour {

    public int speed = 200;
    public float ActiveTime;
    Dictionary<GameObject, RandomCrystal> rc = new Dictionary<GameObject, RandomCrystal>();

    private void Start()
    {
        foreach (Transform item in this.transform)
        {
            if (item.GetComponent<MeshRenderer>() != null)
            {
                //print("R "+gameObject.name);
                //Докиннуть комомнент уппрвлвения рамзером в рандоме
                var d = item.gameObject.AddComponent<RandomCrystal>();
                d.StartScale = item.localScale;
                d.qtStart = item.rotation;
                d.StartScale = Vector3.zero;
                d.step = speed;
            }
        }
    }
}
