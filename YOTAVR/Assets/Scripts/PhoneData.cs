﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneData : MonoBehaviour {

    public List<Sprite> Items;

    public List<PhoneItem> ItemPhone;

    public List<string> CODES_STRING = new List<string>
    {
        "0","1","2","3","4","5","6","7","8","9"
    };

    public GameObject sendButton;
    bool showed = false;

    // Use this for initialization
    void Start () {
        //FindObjectOfType<PhoneItem>().Character = "9";
        var phone = UserData.PHONE;
        if (!string.IsNullOrEmpty(phone))
        {
            int i = 0;
            foreach(var item in phone)
            {
                ItemPhone[i].Character = item.ToString();
                i++;
            }
            /*foreach(var item in ItemPhone)
            {
                item.Character = phone[i].ToString();
                i++;
            } */
        }
	}
	
	// Update is called once per frame
    void Update()
    {
        if (!showed && ItemPhone[ItemPhone.Count - 1].filled)
        {
            sendButton.SetActive(true);
            sendButton.GetComponent<Animation>().Play("SendButtonFadeIn");
            showed = true;
        }
        if (showed && !ItemPhone[ItemPhone.Count - 1].filled)
        {
            sendButton.GetComponent<Animation>().Play("SendButtonFadeOut");
            showed = false;
        }
    }
}
