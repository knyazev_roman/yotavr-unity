﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberSelect : MonoBehaviour {

    public LayerMask mask;
    public float FillSpeed = 1f;
    public GameObject HandleLoader;

    public GameObject LookingFor;
    private float fillAmount;
    private bool IsShown;
    public string character;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000f, mask))
        {
            if (hit.collider.gameObject != LookingFor)
                return;
            //if (drag)
            //    return;
            IsShown = true;
            fillAmount += Time.deltaTime * FillSpeed;
            HandleLoader.GetComponent<Image>().fillAmount = fillAmount / 100;
            if (fillAmount >= 100)
            {
                var items = FindObjectOfType<PhoneData>().ItemPhone;               
                foreach (var item in items)
                {
                    if (!item.filled)
                    {
                        item.Character = character;
                        print(item.name);
                        break;
                    }
                }
                reset();
            }
            //print(fillAmount);
        }
        else if (IsShown)
        {
            reset();
        }
    }

    private void reset()
    {
        IsShown = false;
        fillAmount = 0;
        HandleLoader.GetComponent<Image>().fillAmount = 0;
    }

}
