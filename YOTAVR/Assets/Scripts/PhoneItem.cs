﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhoneItem : MonoBehaviour {

    public int position;
    public bool filled;

    private string character;
    public string Character
    {
        get
        {
            return character;
        }
        set
        {           
            character = value;
            Color c = GetComponent<Image>().color; ;
            if (character != null)
            {
                int index = FindObjectOfType<PhoneData>().CODES_STRING.IndexOf(character);
                var image = FindObjectOfType<PhoneData>().Items[index];
                GetComponent<Image>().sprite = image;
                c.a = 1;
                filled = true;
            }
            else
            {
                c.a = 0;
                filled = false;
            }
            GetComponent<Image>().color = c;
            
            
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void reset()
    {
        Character = null;
        filled = false;
    }
}
