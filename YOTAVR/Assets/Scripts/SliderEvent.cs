﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SliderEvent{

    NONE = -1,
    CITY = 0,
    TARRIF = 1,
    SMS = 2,
    NO_SMS = 3,
    UNLIMITED = 4,
    NO_UNLIMITED = 5, 
    PHONE = 6,
    POLZUNOK = 7,
    GB = 8,

}
