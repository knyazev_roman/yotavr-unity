﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Compas : MonoBehaviour {
    public Text UITest;
    public float speed = 10f;
    public float Timer = 2f;
    public float TimerLeft;
    public Animation AnimClip;
    public GameObject lookat;
    public LayerMask mask;
    public bool IsStartRotate;

    public AudioSource sourceAudio;
    public AudioClip[] Sounds;

    /// <summary>
    /// UI который будет скрываться после выполнения кветса
    /// </summary>
    public SpriteRenderer[] UiHides;
    public bool CustumLogicisDone = false;
    public GameObject SoundParticleEffectFinish;
    public GameObject anotherScenes;

    public GameObject CenterCompass;

    void Start () {
        anotherScenes.SetActive(false);
        anotherScenes.SetActive(true);
        FindObjectOfType<TouchDraw>().CanDraw = false;
        AnimClip["CompasStart"].speed = 0;
        AnimClip.Play("CompasStart");
        TimerLeft = Timer;
    }


    private void Update()
    {

    }

    void Update123()
    {
        var att = Input.gyro.attitude;
        CenterCompass.transform.Rotate(0, 0, att.y);
        if (!FindObjectOfType<TouchDraw>().CanDraw)
            return;
        var t =  FindObjectOfType<GvrHead>().transform;
        var x = t.rotation.x.ToString();
        var y = t.rotation.y.ToString();
        var z = t.rotation.z.ToString();
        UITest.text = string.Format("x={0}, y={1}, z={2}", x,y,z);
        if (IsStartRotate)
            return;
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 2000f, mask))
        {
            TimerLeft -= Time.deltaTime;
            if (TimerLeft <= 0)
            {
                AnimClip["CompasStart"].speed = 1;
                IsStartRotate = true;
            }
        }
        else
        {
            TimerLeft = Timer;
        }
    }
}