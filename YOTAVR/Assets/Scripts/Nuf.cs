﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nuf : MonoBehaviour {

    public LayerMask mask;
    public GameObject NufObject;
    public Animation animation;
    public TimerLoadScene LoadScene;
    // Use this for initialization
    void Start () {
        //animation.enabled = false;
        animation["nuf"].speed = 0;
        animation.Play("nuf");
    }
	
	// Update is called once per frame
	void Update () {
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000f, mask))
        {
            LoadScene.Wait = false;
            //animation.Play("nuf");
            animation["nuf"].speed = 1;
            //animation.enabled = true;
        }
        else
        {
            LoadScene.Wait = true;
            animation["nuf"].speed = 0;
            //animation.enabled = false;
        }   

        /*if (Input.GetKey(KeyCode.A))
        {
            animation.Play("nuf");
        }
        if (Input.GetKey(KeyCode.B))
        {
            animation["nuf"].speed = 0;
        }  */
    }
}
