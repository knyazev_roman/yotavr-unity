﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapMonitor : MonoBehaviour {

    DrawMap dm;
    public UnityEngine.UI.Image Text;
    public AudioClip SoundFinish;
    AudioSource SourceAudio;

    // Use this for initialization
    void Start () {
        dm = this.GetComponent<DrawMap>();
        Text.gameObject.SetActive(false);

        SourceAudio = this.GetComponent<AudioSource>();
    }
    public float timeLeft = 5f;
    bool showIsDone = false;
	// Update is called once per frame
	void Update () {
        if (!showIsDone)
        {
            if (dm.IsDone)
            {
                showIsDone = true;
                Text.gameObject.SetActive(true);
                if(SoundFinish != null){
                    SourceAudio.Stop();
                    SourceAudio.PlayOneShot(SoundFinish);
                }
            }
        }
        else
        {
            timeLeft -= Time.deltaTime;
            if (timeLeft < 0){
                //dm.FastLoad = true;
                this.enabled = false;
                FindObjectOfType<DrawMap>().CallDestroy();
            }
        }
	}
}
