﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PriceItem : MonoBehaviour {

    public int position;
    private string character;
    public string Character
    {
        get
        {
            return character;
        }
        set
        {
            character = value;
            int index = FindObjectOfType<PriceData>().PRICE_STRING.IndexOf(character);
            //print(index);
            //transform.GetChild(index).gameObject.SetActive(true);
            //GetComponent<Image>().sprite = image;
        }
    }
    public bool Show()
    {
        if (character != null)
        {
            int index = FindObjectOfType<PriceData>().PRICE_STRING.IndexOf(character);
            transform.GetChild(index).gameObject.SetActive(true);
            return true;
        }
        else
        {
            return false;
        }
    }
    public void HideItem(int index)
    {
        transform.GetChild(index).gameObject.SetActive(false);
    }
    public string CharacterTemp { get; set; }
    public void Hide()
    {
        for(int i=0;i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }
    // Use this for initialization
    void Start () {
		
	}
}
