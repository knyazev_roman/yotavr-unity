﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour {

    public ScreenOrientation Orientation;

    public void SetVR()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }

    // Use this for initialization
    void Start () {
        UserData.IsFirstLoad = 1;
        Screen.orientation = Orientation;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
