﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerLoadScene : MonoBehaviour {

    public float timeLeft = 7.5f;
    public ScenesData.SCENES Scene;
    public bool Wait;

    private static AsyncOperation operation;
    void Start () {
        if(GetComponent<AudioSource>()!=null)
            GetComponent<AudioSource>().Stop();
        StartCoroutine(LoadScene());
    }
    private IEnumerator LoadScene()
    {
        operation = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync((int)Scene/*, UnityEngine.SceneManagement.LoadSceneMode.Single*/);
        operation.allowSceneActivation = false;
        yield return operation;
    }
    
    void Update () {
        if (Wait)
            return;
        timeLeft -= Time.deltaTime;
        if (timeLeft < 0){
            Invoke("Loaded", 0);
        }
    }

    private void Loaded(){
        operation.allowSceneActivation = true;
    }
}
