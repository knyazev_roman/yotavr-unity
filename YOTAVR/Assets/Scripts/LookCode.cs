﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LookCode : MonoBehaviour {

    private const float SIZE = 3f;

    public LayerMask mask;
    public Material CheckedMaterial;
    public GameObject RayObject;
    public float fillTime = 0.1f;
    private float timer;

    public AudioSource source;
    public AudioClip clipFound;
    public AudioClip clipSet;
    public GameObject target;

    // Use this for initialization
    void Start () {
        source = this.GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        var items = Physics.SphereCastAll(ray, SIZE, 1000f);
        RayObject = null;
        foreach (var hit in items)
        {
            if (hit.collider.GetComponent<CodeItem>() == null) return;

            /*
            if (RayObject != hit.collider.gameObject && source != null && clipFound != null)
            {
                source.PlayOneShot(clipFound);
            }
            if (RayObject != null && RayObject != hit.collider.gameObject)
            {
                RayObject.GetComponent<CodeItem>().ResetItem();
            }
            */
 
            if (RayObject == null)
            {
                //if (source != null && clipFound != null)
                //{
                //    source.PlayOneShot(clipFound);
                //}
                RayObject = hit.collider.gameObject;
            }
        }

        if (RayObject != null)
        {
            //FillItem();

            var tokken = RayObject.GetComponent<CodeItem>();
            if (!tokken.found)
            {
                source.PlayOneShot(clipFound);

                tokken.found = true;
                tokken.source = source;
                tokken.clipSet = clipSet;
                tokken.target = target;
            }

            /*
            if (items.Length == 0)
            {
                ResetForItem();
            }
             */
        }
        /*
        if (items.Length == 0)
        {
            if (RayObject != null)
            {
                Debug.Log(RayObject);
                RayObject.GetComponent<CodeItem>().ResetItem();
            }
        }
         * */
    }

    /*
    void FillItem()
    {
        SetAlpha(0.2f);
        if (timer < fillTime)
        {
            timer += Time.deltaTime;
            SetAlpha(timer / fillTime);
            if(timer >= fillTime)
            {
                FindObjectOfType<OpenCode>().SetCode(RayObject.GetComponent<CodeItem>().position, RayObject.GetComponent<CodeItem>().Character);
                RayObject.layer = 0;
                RayObject.SetActive(false);
                RayObject = null;
                timer = 0;
                source.PlayOneShot(clipSet);
            }
        }
    }
     */

    void SetAlpha(float value)
    {
        var img = RayObject.GetComponentInChildren<SpriteRenderer>();
        var color = img.color;
        color.a = value;
        img.color = color;
    }

    public void ResetForItem()
    {
        timer = 0;
        RayObject = null;
        /*RayObject.GetComponent<CodeItem>().ResetItem();
        return;
        if (timer < fillTime)
        {
            timer += Time.deltaTime;
            SetAlpha(timer / fillTime);
            if (timer >= fillTime)
            {
                //timer = 0;
                //RayObject = null;
            }
        }
           */
       
    }
}
