﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberSelect3D : MonoBehaviour {

    public LayerMask mask;
    //float FillSpeed = 1f;
    //public GameObject HandleLoader;

    public GameObject LookingFor;
    private float fillAmount;
    private bool IsShown;
    public string character;

    public Vector3 StartPosition;
    public float RandomSpeed = 3;

    bool yTurn = true;

    bool floatAngle = true;
    bool leftRignt = false;

    float AngleRot = 0;
    public bool IsClickEvent = true;
    public float timerTouch = 0.5f;


    AudioSource s;
    AudioClip sSelect;
    // Use this for initialization
    void Start()
    {
        StartPosition = this.transform.position;
        RandomSpeed = Random.Range(3, 7);
        leftRignt = Random.Range(0, 1) == 1 ? true : false;
        s = this.gameObject.AddComponent<AudioSource>();
        sSelect = Resources.Load("yota_FX_num_digit_appear") as AudioClip;
        //FillSpeed = 
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000f, mask))
        {
            if (hit.collider.gameObject == LookingFor)
            {
                //if (drag)
                //    return;
                if (!IsShown)
                {
                    Adddigit();
                }

                //print(fillAmount);
            }
        }
        if(fillAmount!=0){
            //this.transform.parent.rotation = Quaternion.identity;
        }
        SmallTurn();
    }

    IEnumerator Rotate()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            var temp = Time.deltaTime * (3 * 70);
            fillAmount += temp;
            print("start rotate");
            print("fill="+fillAmount);
            //HandleLoader.GetComponent<Image>().fillAmount = fillAmount / 100;
            this.transform.parent.Rotate(0, temp, 0);
            if (fillAmount >= 360)
            {
                //print("rotate");
                //var items = FindObjectOfType<PhoneData>().ItemPhone;
                //foreach (var item in items)
                //{
                //    if (!item.filled)
                //    {
                //        item.Character = character;
                //        print(item.name);
                //        break;
                //    }
                //}
                this.transform.parent.rotation = Quaternion.identity;
                reset();
                s.PlayOneShot(sSelect);
                print("break");
                IsShown = false;
                break;
                
            }
        }
        
    }

    void Adddigit()
    {
        bool detected = false;
        timerTouch += Time.deltaTime;
#if UNITY_EDITOR
        if (Input.GetMouseButton(0) && timerTouch > 1)
        {
            timerTouch = 0;
            detected = true;
            //print("1");
        }
#endif
        if (!detected && Input.GetMouseButtonDown(0))
        {
            timerTouch = 0;
            detected = true;
            //print("2");
        }
        if (!detected && Input.touchCount > 0 && timerTouch > 1)
        {
            timerTouch = 0;
            detected = true;
            //print("3");
        }
        bool doneEvent = true;
        if (IsClickEvent && !detected)
            doneEvent = false;
        if (!doneEvent)
            return;
        print("Add");
        IsShown = true;

        print("rotate");

        var items = FindObjectOfType<PhoneData>().ItemPhone;
        foreach (var item in items)
        {
            if (!item.filled)
            {
                item.Character = character;
                print(item.name);
                break;
            }
        }

        StartCoroutine(Rotate());
    }


    void SmallTurn()
    {
        //return;
        if (IsShown)
            return;
        reset();
        if(yTurn)
        {
            if (transform.position.y <= (StartPosition.y + 5))
                transform.position += new Vector3(0, RandomSpeed * Time.deltaTime, 0);
            else
            {
                yTurn = false;
            }
        }
        else
        {
            if (transform.position.y >= (StartPosition.y - 5))
                transform.position -= new Vector3(0, RandomSpeed * Time.deltaTime, 0);
            else
            {
                yTurn = true;
            }
        }

        if(leftRignt)
        {
            AngleRot += RandomSpeed * Time.deltaTime;
            this.transform.parent.Rotate(0, RandomSpeed * Time.deltaTime, 0);
            if (AngleRot >= 5)
            {
                //AngleRot = 0;
                leftRignt = false;
            }
        }
        else
        {
            AngleRot -= RandomSpeed * Time.deltaTime;
            this.transform.parent.Rotate(0, -RandomSpeed * Time.deltaTime, 0);
            if (AngleRot <= -5)
            {
                //AngleRot = 0;
                leftRignt = true;
            }
        }

    }

    private void reset()
    {
        IsShown = false;
        fillAmount = 0;

        //HandleLoader.GetComponent<Image>().fillAmount = 0;
    }
}
