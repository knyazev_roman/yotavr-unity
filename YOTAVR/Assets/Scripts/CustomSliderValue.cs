﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CustomSliderValue  {
    public int Value;
    public int GB;
    public int Minutes;

    public int MINSum;
    public int GBSumm;
    public int Social;
    public int Message;

    public CustomSliderValue() { }
    public CustomSliderValue(UserData.CityClass cityValues)
    {
        Value = cityValues.Price;
        GB = cityValues.GB;
        Minutes = cityValues.MIN;

        MINSum = cityValues.MINSum;
        GBSumm = cityValues.GBSumm;
        Social = cityValues.Social;
        Message = cityValues.Message;
    }
}
