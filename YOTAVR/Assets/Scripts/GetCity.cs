﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class GetCity : MonoBehaviour {
    public Text TextField;
    public float Lat = 56.326887f;
    public float Lng = 44.005986f;
    private string url = "https://maps.googleapis.com/maps/api/geocode/json?language=ru&latlng=";


    public class AddressComponent
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public List<string> types { get; set; }
    }

    public class Location
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Northeast
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Southwest
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Viewport
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }

    public class Northeast2
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Southwest2
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Bounds
    {
        public Northeast2 northeast { get; set; }
        public Southwest2 southwest { get; set; }
    }

    public class Geometry
    {
        public Location location { get; set; }
        public string location_type { get; set; }
        public Viewport viewport { get; set; }
        public Bounds bounds { get; set; }
    }

    public class Result
    {
        public List<AddressComponent> address_components { get; set; }
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string place_id { get; set; }
        public List<string> types { get; set; }
    }

    public class RootObject
    {
        public List<Result> results { get; set; }
        public string status { get; set; }
    }

    public void GetCityName(float Lat, float Lng)
    {
        TextField.text = "DONE-1";
        this.Lat = Lat;
        this.Lng = Lng;
        StartCoroutine(Parse());
        //Parse();
    }

    void Start()
    {

    }

    
    // Use this for initialization
    IEnumerator Parse () {
        
        //TextField.text = "DONE0";
        url += string.Format("{0},{1}", Lat, Lng);
        WWW www = new WWW(url);
        yield return www;
        if (www.error == null)
        {
            Processjson(www.text);
        }
        else
        {
            TextField.text = "ERROR: " + www.error;
            Debug.Log("ERROR: " + www.error);
            FindObjectOfType<Geolocation>().ShowCities();
        }
    }

    private void Processjson(string data)
    {
        try
        {
            TextField.text = "DONE";
            RootObject root = new RootObject();//SimpleJSON.JSON.Parse(data) //JsonConvert.DeserializeObject<RootObject>(data);
            var address_components = root.results[0].address_components[2];
            print(address_components.long_name);
            TextField.text = address_components.long_name;
            //var cities = FindObjectsOfType<CityItem>();
            bool isFind = false;            
            var cities = FindObjectOfType<Geolocation>().Cities.gameObject.GetComponentsInChildren<CityItem>();
            TextField.text = cities.Length.ToString();
            foreach (var city in cities)
            {
                if (address_components.long_name.Trim().ToLower().Contains(city.title.Trim().ToLower()) 
                    || address_components.short_name.Trim().ToLower().Contains(city.title.Trim().ToLower()))
                {
                    
                    isFind = true;
                    break;
                }
            }
            if (isFind)
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene((int)FindObjectOfType<Geolocation>().Scene);
            }
            else
            {
                FindObjectOfType<Geolocation>().ShowCities();
            }
        }
        catch(Exception ex)
        {
            TextField.text = ex.Message;
            FindObjectOfType<Geolocation>().ShowCities();
        }
        
    }


}
