﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts;
using UnityEngine.UI;

public class ConnectionDone : MonoBehaviour
{
	public GameObject errorPopUp;
	private Text errorText;

    void Start()
    {
        string url = "";
        string urlCode = "";
        string getStatus = "";

        //Test();

        foreach (var p in DictParam.dictServer)
        {
            p.Value.TryGetValue("url", out url);
            p.Value.TryGetValue("code", out urlCode);
            p.Value.TryGetValue("get", out getStatus);
            StartCoroutine(Parse(url, getStatus, urlCode));
        }

		errorText = errorPopUp.GetComponentInChildren<Text> ();
    }

    private void Test()
    {
        UserData.City = "sdsadas";
        UserData.PHONE = "dsadsadsad";
        UserData.TarrifMinute = 1000;
        UserData.TarrifMinSum = 15;
        UserData.TarrifGB = 1000;
        UserData.TarrifGBPrice = 15;
        UserData.SMS = 1;
        UserData.FACEBOOK = 1;
        UserData.INSTAGRAM = 1;
        UserData.MESSAGE = 1;
        UserData.OK = 1;
        UserData.SKYPE = 1;
        UserData.TELEGRAM = 1;
        UserData.TWITTER = 1;
        UserData.VIBER = 1;
        UserData.VK = 1;
        UserData.WHATSAPP = 1;
        UserData.TarrifTotalSum = 150;
    }

    private string getGetParam()
    {
        string result = "/?code=" + UserData.PROMO;
        foreach (KeyValuePair<string, string> p in DictParam.dict)
        {
            result += p.Key == DictParam.dict.First().Key ? "" : "&" + p.Key + "=" + p.Value;
        }
        return result;
    }

    private WWWForm getPostParam()
    {
        WWWForm wwwForm = new WWWForm();
        foreach (KeyValuePair<string, string> p in DictParam.dict)
        {
            wwwForm.AddField(p.Key, p.Value);
        }
        return wwwForm;
    }

    IEnumerator Parse(string url, string getStatus, string urlCode)
    {
        if (string.IsNullOrEmpty(UserData.PROMO))
        {
            WWW wwwCode = new WWW(urlCode);
            yield return wwwCode;
            if (wwwCode.error == null)
            {
                try
                {
                    var json = JsonUtility.FromJson<JsonServer>(wwwCode.text);
                    if (json.state)
                    {
                        UserData.PROMO = json.code;
                        DictParam.dict["code"] = json.code;
                    }
                }
                catch (System.Exception ex)
                {
                    print(ex.Message);
                }
            }
        }

        if (!string.IsNullOrEmpty(getStatus))
        {
            WWW www = new WWW(url + getGetParam());
            yield return www;
            if (string.IsNullOrEmpty(www.error))
            {
                Done();
            }
            else
            {
                www = new WWW(url, getPostParam());
				if (string.IsNullOrEmpty (www.error)) {
					Done ();
				} else {
					Debug.Log ("ERROR1");
					errorText.text = www.error;
					errorPopUp.SetActive (true);
					StartCoroutine (DoneCoroutine ());
				}
            }
        }
        else
        {
            WWW www = new WWW(url, getPostParam());
            if (string.IsNullOrEmpty(www.error))
            {
                Done();
			} else {
				errorText.text = www.error;
				errorPopUp.SetActive (true);
				StartCoroutine (DoneCoroutine ());
				Debug.Log ("ERRO2");
			}
        }
    }

    private void Done()
    {
        UserData.DelteAllKey();
        UserData.DeletePromo();

        FindObjectOfType<TimerLoadScene>().Wait = false;
        try
        {
            print("send state is send");
        }
        catch (System.Exception ex)
        {
            print(ex.Message);
        }

    }

	#region Coroutine
	IEnumerator DoneCoroutine()
	{
		yield return new WaitForSeconds (3);
		Done ();
	}
	#endregion
}

static class DictParam
{
    public static Dictionary<string, string> dict;
    public static Dictionary<string, Dictionary<string, string>> dictServer;
    static DictParam()
    {
        dict = new Dictionary<string, string>()
        {
            { "code",UserData.PROMO },
            { "phone", WWW.EscapeURL(UserData.PHONE, Encoding.UTF8) },
			{ "city", UserData.City}, //город
            { "minCount", WWW.EscapeURL(UserData.TarrifMinute.ToString(), Encoding.UTF8) }, //кол-во минут
            { "minPrice", WWW.EscapeURL(UserData.TarrifMinSum.ToString(), Encoding.UTF8) }, //стоимость минут
            { "gbCount", WWW.EscapeURL(UserData.TarrifGB.ToString(), Encoding.UTF8) }, //кол-во ГБ
            { "gbPrice", WWW.EscapeURL(UserData.TarrifGBPrice.ToString(), Encoding.UTF8) }, //стоимость ГБ
            { "unlimitedSMS", UserData.SMS == 1 ? "true" : "false" }, //безлимитные смс
            { "facebook", UserData.FACEBOOK == 1 ? "true" : "false"},
            { "instagram", UserData.INSTAGRAM == 1 ? "true" : "false"},
            { "message", UserData.MESSAGE == 1 ? "true" : "false"},
            { "ok", UserData.OK == 1 ? "true" : "false"},
            { "skype", UserData.SKYPE == 1 ? "true" : "false"},
            { "telegram", UserData.TELEGRAM == 1 ? "true" : "false"},
            { "twitter", UserData.TWITTER == 1 ? "true" : "false"},
            { "viber", UserData.VIBER == 1 ? "true" : "false"},
            { "vk", UserData.VK == 1 ? "true" : "false"},
            { "whatsapp", UserData.WHATSAPP == 1 ? "true" : "false"},
            { "totalSumm",(UserData.TarrifGBPrice + UserData.Social + (UserData.SMS == 1 ? 50 : 0)).ToString()},
        };
        dictServer = new Dictionary<string, Dictionary<string, string>>()
        {
            { "sa-wd.ru", new Dictionary<string, string>()
                {
                    { "url","https://sa-wd.ru/yota/register" },
                    { "code", "https://sa-wd.ru/yota" },
                    { "get", "true" }
                }
            },
            { "localhost", new Dictionary<string, string>()
                {
                    { "url","https://localhost/yota/register" },
                    { "code", "https://sa-wd.ru/yota" },
                    { "get", "" }
                }
            },
            { "91.144.161.208", new Dictionary<string, string>()
				{
					//{ "url","http://192.168.0.165:5000/tariffsettings" },
                    { "url","http://91.144.161.208:1444/tariffsettings" },
                    { "code", "https://sa-wd.ru/yota" },
                    { "get", "" }
                }
            }
        };
    }
}