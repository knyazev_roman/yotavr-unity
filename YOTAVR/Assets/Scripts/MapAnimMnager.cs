﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapAnimMnager : MonoBehaviour {

    public GameObject NextScene;
    public float Timer = 7f;

    public void MapFinish()
    {
        var anim = GetComponent<Animation>();
        anim.clip = anim.GetClip("MapFinish");
        anim.Play();
        StartCoroutine(MapFinishAudio());
    }

    public IEnumerator MapFinishAudio()
    {
        while (true)
        {
            Timer -= Time.deltaTime;
            if (Timer < 0)
                break;
            yield return new WaitForEndOfFrame();
        }
        print("END");
        this.gameObject.SetActive(false);
        NextScene.SetActive(true);
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
