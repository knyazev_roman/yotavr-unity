﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurveSlider : MonoBehaviour
{

    public float minAngle;
    public float maxAngle;
    public Text UIText;
    public Slider slider;
    public GameObject Handle;
    public Transform player;
    public bool drag;
    public List<CustomSliderValue> SliderValues;
    public CustomSliderValue SliderValue;
    public ShowPrice Price4;
    private int lastValue = 0;
    private int target = 0;
    public float speed = 5f;
    public GameObject point;
    public Collider sliderCollider;
    public GameObject Numbers;
    public bool isMin;

    private float GetOffset(float i)
    {
        float offset = (i * 20) / slider.maxValue;
        if (i / slider.maxValue > 0.5f)
        {
            offset = ((slider.maxValue - i) * 20) / slider.maxValue;
        }

        return offset - 4;
    }

    public void SetPosition()
    {
        //float offset = (slider.value * 10) / slider.maxValue;
        //if(slider.value / slider.maxValue > 0.5f)
        //{
        //    offset = ((slider.maxValue - slider.value) * 10) / slider.maxValue;
        //}
        float offset = GetOffset(slider.value);
        Handle.GetComponent<RectTransform>().offsetMin = new Vector2(Handle.GetComponent<RectTransform>().offsetMin.x, -offset);
        Handle.GetComponent<RectTransform>().offsetMax = new Vector2(Handle.GetComponent<RectTransform>().offsetMax.x, -offset);
    }
    // Use this for initialization
    void Start()
    {
        slider.maxValue = UserData.MyCity.Count - 1;
        float segment = GetComponent<RectTransform>().sizeDelta.x / (UserData.MyCity.Count - 1);

        Vector3 start = transform.position;
        start.x += GetComponent<RectTransform>().sizeDelta.x / 2;

        SliderValues.Clear();
        for (int i = 0; i < UserData.MyCity.Count; i++)
        {
            SliderValues.Add(new CustomSliderValue(UserData.MyCity[i]));

            Vector3 pos = start;
            pos.x -= segment * i;
            if(i == slider.maxValue/2) pos.y -= GetOffset(i) - 1.6f;
            else if (i == 0 || i == slider.maxValue) pos.y -= GetOffset(i);
            else pos.y -= GetOffset(i) + 1.6f;

            GameObject newPoint = Instantiate(point, pos, Quaternion.identity);
            newPoint.transform.parent = transform.GetChild(0).transform;           
        }

        SliderValue = SliderValues[0];
        ShowData();
    }
    void ShowData()
    {
        Price4.SetPrice(SliderValue);
    }

    // Update is called once per frame
    void Update()
    {
        // Катаем слайдер
        slider.value += (target - slider.value) * (Time.deltaTime * speed);
        SliderValue = SliderValues[target];
        if (Mathf.Abs(slider.value - target) >= 0.5f)
        {
            ShowData();
        }

        RaycastHit hit;
        if (Physics.Raycast(player.position, player.forward, out hit, 1000f))
        {
            if (hit.collider != sliderCollider) return;
        }
        else return;

        if (!Input.GetButton("Tap"))
            return;

        if (player.localEulerAngles.y < minAngle || player.localEulerAngles.y > maxAngle)
            return;

        var percent = (float)(System.Math.Round((player.localEulerAngles.y - minAngle) / (maxAngle - minAngle), 2)) * slider.maxValue;
        target = (int)System.Math.Round(percent);

        //Debug.Log(player.localEulerAngles.y);
        //Debug.Log("" + (percent * 100) + " %");

        if (target < 0) target = 0;
        if (target > SliderValues.Count - 1) target = SliderValues.Count - 1;
    }
}
