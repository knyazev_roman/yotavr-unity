﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchManager : MonoBehaviour {

    public GameObject SwitcherOn;
    public GameObject SwitcherOff;

    // Use this for initialization
    void Start () {
		if(UserData.UNLIMITED == 1)
        {
            SwitcherOn.SetActive(true);
        }
        else
        {
            SwitcherOff.SetActive(true);
        }
         
	}
	
	// Update is called once per frame
	void Update () {

	}
}
