﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallTurn : MonoBehaviour {

    bool xTurn = true;

    bool floatAngle = true;
    bool upDown = false;

    float AngleRot = 0;

    public Vector3 StartPosition;
    public float RandomSpeed = 3;

    // Use this for initialization
    void Start () {
        StartPosition = this.transform.position;
        RandomSpeed = Random.Range(3, 7);
        upDown = Random.Range(0, 1) == 1 ? true : false;
    }

    void Update()
    {
        if (xTurn)
        {
            if (transform.position.x <= (StartPosition.x + 5))
                transform.position += new Vector3(RandomSpeed * Time.deltaTime, 0, 0);
            else
            {
                xTurn = false;
            }
        }
        else
        {
            if (transform.position.x >= (StartPosition.x - 5))
                transform.position -= new Vector3(RandomSpeed * Time.deltaTime, 0, 0);
            else
            {
                xTurn = true;
            }
        }

        if (upDown)
        {
            AngleRot += RandomSpeed * Time.deltaTime;
            this.transform.parent.Rotate(RandomSpeed * Time.deltaTime, 0, 0);
            if (AngleRot >= 5)
            {
                //AngleRot = 0;
                upDown = false;
            }
        }
        else
        {
            AngleRot -= RandomSpeed * Time.deltaTime;
            this.transform.parent.Rotate(-RandomSpeed * Time.deltaTime, 0, 0);
            if (AngleRot <= -5)
            {
                //AngleRot = 0;
                upDown = true;
            }
        }
    }

    // Update is called once per frame
    void Update2 () {
        if (xTurn)
        {
            if (transform.position.y <= (StartPosition.y + 5))
                transform.position += new Vector3(0, RandomSpeed * Time.deltaTime, 0);
            else
            {
                xTurn = false;
            }
        }
        else
        {
            if (transform.position.y >= (StartPosition.y - 5))
                transform.position -= new Vector3(0, RandomSpeed * Time.deltaTime, 0);
            else
            {
                xTurn = true;
            }
        }

        if (upDown)
        {
            AngleRot += RandomSpeed * Time.deltaTime;
            this.transform.parent.Rotate(0, RandomSpeed * Time.deltaTime, 0);
            if (AngleRot >= 5)
            {
                //AngleRot = 0;
                upDown = false;
            }
        }
        else
        {
            AngleRot -= RandomSpeed * Time.deltaTime;
            this.transform.parent.Rotate(0, -RandomSpeed * Time.deltaTime, 0);
            if (AngleRot <= -5)
            {
                //AngleRot = 0;
                upDown = true;
            }
        }
    }
}
