﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DrawMap : MonoBehaviour {

    public int NeedDrawed;
    public Text CountDraw;
    public ScenesData.SCENES Scene;
    public LayerMask mask;
    public bool draw;
    public bool IsDone;
    public bool FastLoad = true;
    public List<GameObject> NeededDraw;
    public DoneType TypeDone;
    public bool UseDestroy = false;
    public bool CustumLogicisDone = false;
    /// <summary>
    /// UI который будет скрываться после выполнения кветса
    /// </summary>
    public UnityEngine.UI.Image[] UiHides;
    public StepObject step;
    public enum DoneType
    {
        COUNT = 0,
        SHOWING = 1,
    }

    public AudioSource sourceAudio;
    public AudioClip[] Sounds;

    BoxCollider[] objs = null;
    public GameObject SoundParticleEffectFinish;

    public GameObject anotherScenes;


    void Start () {
        anotherScenes.SetActive(false);
        anotherScenes.SetActive(true);

        FindObjectOfType<TouchDraw>().CanDraw = false;

        //StartCoroutine(Init());
        objs = FindObjectsOfType<BoxCollider>();
        int c = 0;
        foreach (var item in objs)
        {
            item.transform.GetChild(0).gameObject.AddComponent<CrystalBaseItem>();
            var d = item.transform.GetChild(0).gameObject.AddComponent<SelfDraw>();
            d.Index = c + 1;
            SelfDraws.Add(d);
            c++;
        }
    }


    public List<SelfDraw> SelfDraws;
    public int DrawIndex;
    private IEnumerator DrawSelf()
    {
        while (!IsDone)
        {
            if (DrawIndex >= SelfDraws.Count)
            {
                DrawIndex = 0;
            }
            else
            {
                DrawIndex++;
            }
            foreach (var item in SelfDraws)
            {
                if (item.Index == DrawIndex)
                {
                    item.gameObject.SetActive(true);
                    item.transform.gameObject.layer = 0;
                    print("dd");
                    NeedDrawed--;
                    //item.transform.GetChild(0).gameObject.layer = 0;
                    //item.transform.transform.GetChild(0).gameObject.SetActive(true);
                    //IsStartDraw = false;
                    yield return new WaitForSeconds(0.3f);
                    break;
                }
            }
            //break;
            //IsDone = CheckDone();
        }
    }
    bool CheckDone()
    {
        foreach (var item in NeededDraw)
        {
            if (item.layer != 0)
                return false;
        }
        return true;
    }

    bool _oCall = false;
    public void CallDestroy()
    {
        if (_oCall) return;
        _oCall = true;
        draw = false;
        int c = 0;

        foreach (CrystalBaseItem item in FindObjectsOfType<CrystalBaseItem>())
        {
            if(!item.transform.GetChild(0).gameObject.activeSelf)
            {
                if (item.transform.GetChild(0).gameObject.GetComponent<BoxCollider>() != null)
                    item.transform.GetChild(0).gameObject.GetComponent<BoxCollider>().enabled = false;
            }
        }


        var items = FindObjectsOfType<RandomCrystal>();
        foreach(var item in items)
        {
            item.enabled = false;
            var MeshItem = item.GetComponent<MeshRenderer>();
            item.gameObject.AddComponent<BoxCollider>();
            //item.GetComponent<BoxCollider>().isTrigger = true;
            MeshItem.gameObject.AddComponent<Rigidbody>();
            MeshItem.gameObject.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * 675 * -1, ForceMode.Acceleration);
            MeshItem.gameObject.AddComponent<LowTrans>();
        }
        if (SoundParticleEffectFinish != null)
            SoundParticleEffectFinish.gameObject.SetActive(true);
        print(items.Length);


        return;
        foreach (BoxCollider item in objs)
        {
            if (item.transform.GetChild(0).transform.parent.GetComponent<BoxCollider>() == null)
            {
                print("Not Box");
                continue;
            }

            item.transform.GetChild(0).gameObject.SetActive(true);
            item.transform.GetChild(0).transform.parent.GetComponent<BoxCollider>().enabled = false;

            foreach (Transform MeshItem in item.transform.GetChild(0).gameObject.transform)
            {
                if (MeshItem.GetComponent<MeshRenderer>() != null)
                {
                    if (MeshItem.gameObject.GetComponent<RandomCrystal>() != null)
                    {
                        MeshItem.gameObject.GetComponent<RandomCrystal>().enabled = false;
                        
                    }
                }
            }
            //print(item.gameObject.name+" c="+c);
            

            foreach (Transform MeshItem in item.transform.GetChild(0).gameObject.transform)
            {
                if (MeshItem.GetComponent<MeshRenderer>() != null)
                {

                    MeshItem.gameObject.AddComponent<BoxCollider>();
                    MeshItem.gameObject.AddComponent<Rigidbody>();
                    MeshItem.gameObject.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * 675 * -1, ForceMode.Acceleration);
                    MeshItem.gameObject.AddComponent<LowTrans>();
                    c++;
                    /*if (MeshItem.gameObject.GetComponent<RandomCrystal>() != null)
                    {
                        MeshItem.gameObject.GetComponent<RandomCrystal>().enabled = false;
                        c--;
                    }*/
                }
            }
            //c++;
        }
        print(c);
    }

    IEnumerator LoadScene()
    {
        print("load");
        float fadeTime = FindObjectOfType<Fading>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime);
        FindObjectOfType<TimerLoadScene>().Wait = false;
        //SceneManager.LoadScene((int)Scene);
    }
    public bool IsStartDraw;
    private float TimerLeft;
    public float Timer = 2f;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Tap"))
            draw = true;
        else
            draw = false;
        //if (!FindObjectOfType<TouchDraw>().CanDraw)
        //    return;
        if (IsDone)
        {
            this.enabled = false;
            //StartCoroutine(DrawSelf());
            FindObjectOfType<MapAnimMnager>().MapFinish();
            return;
        }
        /*if (IsDone && FastLoad)
        {
            StartCoroutine(LoadScene());
        }
        if (UseDestroy && IsDone)
        {
            CallDestroy();
        }*/

        //if (IsDone && FastLoad && !CustumLogicisDone)
        //{
        //    SceneManager.LoadScene((int)Scene);
        //}

        if (IsDone && !FastLoad && CustumLogicisDone)
        {
            step.IsDone = true;
            return;
        }

        if (!draw)
            return;
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        var items = Physics.SphereCastAll(ray, 1f, 1000f);
        print(items.Length);
        foreach (var hit in items)
        {

            if (hit.collider.CompareTag("Exit"))
                continue;
            if (hit.collider.transform.gameObject.layer == 0)
                continue;
            hit.collider.transform.gameObject.layer = 0;
            hit.collider.transform.GetChild(0).gameObject.layer = 0;
            hit.collider.transform.GetChild(0).gameObject.SetActive(true);
            sourceAudio.PlayOneShot(Sounds[UnityEngine.Random.Range(0, Sounds.Length)]);
            NeedDrawed--;
            IsStartDraw = false;
        }
        if (IsStartDraw)
        {
            TimerLeft -= Time.deltaTime;
        }
        print(items.Length);
        if (items.Length==0)
        {
            IsStartDraw = true;
            if (TimerLeft <= 0 && IsStartDraw){
                TimerLeft = Timer;
               StartCoroutine( DrawSelf());
            }
        }
        int t = 0;
        foreach (var item in NeededDraw)
        {
            if (item.layer != 0)
                t++;
        }
        if(TypeDone == DoneType.COUNT)
        {
            if (NeedDrawed <= 0)
            {
                IsDone = true;
                foreach (var item in SelfDraws)
                {
                    item.gameObject.SetActive(true);
                }
                return;
            }
            else
                return;
        }
        else
        {
            foreach (var item in NeededDraw)
            {
                if (item.layer != 0)
                    return;
            }
        }
        IsDone = true;
    }

}
