﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OpenCode : MonoBehaviour {

    public GameObject pointer;
    public GameObject Done;
    public Text Codes;
    //public List<GameObject> Codes;
    public AudioClip clip;
    public AudioControllerYota yotaAudio;
    public Animation CloseScaner;

    public Cell cell;

    public List<string> CODES_STRING = new List<string>
    {
        "0","1","2","3","4","5","6","7","8","9",
        "A","B","C","D","E","F","G","H","I","G",
        "K","L","M","N","O","P","Q","R","S","T",
        "U","V", "W","X","Y","Z"
    };

    public List<Sprite> CODES_SPRITE;

    // Use this for initialization
    void Start () {
        cell = FindObjectOfType<Cell>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetCode(int position, string character)
    {
        cell.Timer = Cell.TimerSearch;
        var texts = Codes.text.ToCharArray();
        var t = character;
        var tt = t.ToCharArray()[0];
        print(t);
        print(tt);
        print(Codes.text);
        texts[position] = character.ToCharArray()[0];
        string temp = "";
        foreach(var item in texts)
        {
            temp += item;
        }
        print(temp);
        Codes.text = temp;
        if (IsDone())
        {
            UserData.PROMO = FindObjectOfType<Cell>().CODE;
            pointer.SetActive(true);
            Done.SetActive(true);
            CloseScaner.enabled = true;
        }
    }

    bool IsDone()
    {
        int count = 0;
        print(Codes.text);
        var texts = Codes.text.ToCharArray();
        print(texts.ToString());
        foreach (var item in texts)
        {
            print(item);
            if (item != '*')
            {
                count++;
            }
        }
        if(count == 2){
            yotaAudio.StartPlayAudio(clip);
        }
        print(count);
        foreach(var item in texts)
        {
            if (item == '*')
                return false;
        }
        return true;
    }
}
