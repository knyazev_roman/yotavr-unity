﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DrawUnlimited : MonoBehaviour {

    public ScenesData.SCENES Scene;
    public LayerMask mask;
    bool draw;
    public bool IsDone;
    public bool FastLoad = true;
    public bool IsStartDraw;
    public List<GameObject> NeededDraw;
    public List<SelfDraw> SelfDraws;
    public int DrawIndex;

    public DoneType TypeDone;
    public float Timer = 2f;
    public float TimerLeft;
    public bool UseDestroy = false;
    public bool IsDebug = false;

    public StepObject step;


    public AudioSource sourceAudio;
    public AudioClip[] Sounds;
    public GameObject SoundParticleEffectFinish;

    public enum DoneType
    {
        COUNT = 0,
        SHOWING = 1,
    }


    public GameObject anotherScenes;


    void Start()
    {
        anotherScenes.SetActive(false);
        anotherScenes.SetActive(true);

        FindObjectOfType<TouchDraw>().CanDraw = false;


        /*var Selfs = FindObjectsOfType<SelfDraw>();
        foreach (var item in Selfs)
        {
            SelfDraws.Add(item);
        }*/
        /*foreach (GameObject item in NeededDraw)
        {
            item.transform.GetChild(0).gameObject.AddComponent<CrystalBaseItem>();
        } */

        foreach (SelfDraw item in SelfDraws)
        {
            item.transform.GetChild(0).gameObject.AddComponent<CrystalBaseItem>();
        }
        if (IsDebug)
        {
            foreach (SelfDraw item in SelfDraws)
            {
                item.transform.GetChild(0).gameObject.SetActive(true);
            }
        }
    }

    bool _oCall = false;
    public void CallDestroy()
    {
        if (_oCall) return;
        _oCall = true;
        foreach (SelfDraw item in SelfDraws)
        {
            item.transform.GetChild(0).gameObject.SetActive(true);
            item.transform.GetChild(0).transform.parent.GetComponent<BoxCollider>().enabled = false;
            foreach (Transform MeshItem in item.transform.GetChild(0).gameObject.transform)
            {
                if (MeshItem.GetComponent<MeshRenderer>() != null)
                {
                    MeshItem.gameObject.AddComponent<BoxCollider>();
                    MeshItem.gameObject.AddComponent<LowTrans>();
                    MeshItem.gameObject.AddComponent<Rigidbody>();
                    MeshItem.gameObject.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * 675 * -1, ForceMode.Acceleration);
                    if (MeshItem.gameObject.GetComponent<RandomCrystal>() != null)
                    {
                        MeshItem.gameObject.GetComponent<RandomCrystal>().enabled = false;
                    }
                }
            }
        }
        if (SoundParticleEffectFinish != null)
            SoundParticleEffectFinish.gameObject.SetActive(true);
    }

    public bool CustumLogicisDone = false;
    // Update is called once per frame
    void Update()
    {
        //if (!FindObjectOfType<TouchDraw>().CanDraw)
        //    return;

        if (Input.GetButton("Tap"))
            draw = true;
        else
            draw = false;
        
        if (IsDebug && Input.GetKeyDown(KeyCode.W))
        {
            foreach (SelfDraw item in SelfDraws)
            {
                item.transform.GetChild(0).gameObject.SetActive(true);
                item.transform.GetChild(0).transform.parent.GetComponent<BoxCollider>().enabled = false;
                foreach (Transform MeshItem in item.transform.GetChild(0).gameObject.transform)
                {
                    if (MeshItem.GetComponent<MeshRenderer>() != null)
                    {

                        MeshItem.gameObject.AddComponent<BoxCollider>();
                        MeshItem.gameObject.AddComponent<Rigidbody>();
                        MeshItem.gameObject.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward*450*-1, ForceMode.Acceleration);
                        MeshItem.gameObject.AddComponent<LowTrans>();
                        if (MeshItem.gameObject.GetComponent<RandomCrystal>()!=null){
                            MeshItem.gameObject.GetComponent<RandomCrystal>().enabled = false;
                        }
                    }
                }

            }
        }
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        if (!Physics.Raycast(ray, out hit, 1000f, mask))
        {
            if (TimerLeft <= 0 && IsStartDraw)
            {
                StartCoroutine(DrawSelf());
                TimerLeft = Timer;
            }
        }

        if (IsDone)
        {
            //var audioSources = FindObjectsOfType<AudioSource>() as AudioSource[];
            //foreach (AudioSource item in audioSources)
            //{
            //    item.Stop();
            //}
            //sourceAudio.Stop();
            this.GetComponent<AudioControllerYota>().enabled = false;
            FindObjectOfType<UnlimitedAnimMnager>().UnlimitedFinish();
            this.enabled = false;
            return;
        }
        if (IsDebug) return;
        if (IsDone && FastLoad && !CustumLogicisDone){
            //SceneManager.LoadScene((int)Scene);
            StartCoroutine(LoadScene());
        }
        if (IsDone && FastLoad && CustumLogicisDone){
            step.IsDone = true;
            return;
        }

        if (UseDestroy && IsDone){
            CallDestroy();
        }
        if (!draw)
            return;

        if (IsStartDraw)
        {
            TimerLeft -= Time.deltaTime;
        }

        if (Physics.Raycast(ray, out hit, 1000f))
        {
            if (hit.collider.CompareTag("Exit"))
                return;
            //TimerLeft = Timer;
            //print(hit.transform.gameObject.name);
        }
        if (Physics.Raycast(ray, out hit, 1000f, mask))
        {
            if(hit.collider.GetComponent<SelfDraw>()!=null && hit.collider.transform.GetChild(0).gameObject.activeInHierarchy)
            {
                TimerLeft = Timer;
            }
            DrawIndex = hit.collider.GetComponent<SelfDraw>().Index;
            IsStartDraw = true;
            hit.collider.transform.gameObject.layer = 0;
            hit.collider.transform.GetChild(0).gameObject.layer = 0;
            hit.collider.transform.GetChild(0).gameObject.SetActive(true);
            sourceAudio.PlayOneShot(Sounds[Random.Range(0, Sounds.Length)]);
        }
        else
        {
            if (TimerLeft <= 0 && IsStartDraw)
            {
                //draw = false;
                StartCoroutine(DrawSelf());
                TimerLeft = Timer;
            }
        }

        IsDone = CheckDone();
    }

    IEnumerator LoadScene()
    {
        print("load");
        float fadeTime = FindObjectOfType<Fading>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime);
        FindObjectOfType<TimerLoadScene>().Wait = false;
        //SceneManager.LoadScene((int)Scene);
    }

    private IEnumerator DrawSelf()
    {
        while (!IsDone)
        {
            
            if (DrawIndex >= SelfDraws.Count)
            {
                DrawIndex = 0;
            }
            else
            {
                DrawIndex++;
            }
            
            //print("DI:" + DrawIndex + " COUNT:" + SelfDraws.Count);
            foreach (var item in SelfDraws)
            {
                if (item.Index == DrawIndex)
                {
                    item.transform.gameObject.layer = 0;
                    item.transform.GetChild(0).gameObject.layer = 0;
                    item.transform.transform.GetChild(0).gameObject.SetActive(true);

                    yield return new WaitForSeconds(0.3f);
                }
            }
 
            IsDone = CheckDone();
            //print(IsDone);
        }
    }

    bool CheckDone()
    {
        foreach (var item in NeededDraw)
        {
            if (item.layer != 0)
                return false;
        }
        return true;
    }
}
