﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdvantageManager : MonoBehaviour {

    public DrawUnlimited Unlimited;
    public float Timer = 15;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Timer <= 0)
        {
            //Unlimited.draw = true;
            this.enabled = false;
        }
        Timer -= Time.deltaTime;
	}
}
