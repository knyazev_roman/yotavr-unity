﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SliderDoneEvent : MonoBehaviour {
    public Slider slider;
    public ScenesData.SCENES Scene;
    public SliderEvent MyEvent = SliderEvent.NONE;
    public AudioClip sound;
    public LayerMask mask;
    AudioSource AudioSource;
    public float Timer = 1f;
    private float TimerLeft;
    // Use this for initialization
    void Start () {
        AudioSource = this.GetComponent<AudioSource>();

    }

    public bool isHover = false;
    private void Update()
    {
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 2000f, mask))
        {
            if (TimerLeft > Timer && sound != null && isHover == false)
            {
                isHover = true;
                if (hit.transform.gameObject == this.gameObject && !AudioSource.isPlaying)
                    AudioSource.PlayOneShot(sound);
            }
            if(Input.GetButton("Tap") && hit.transform.gameObject == this.gameObject)
            {
                SceneManager.LoadScene((int)Scene);
            }
        }
        else
        {
            isHover = false;
        }
        TimerLeft += Time.deltaTime;
    }

    public void CheckData()
    {
        if (slider.value == slider.maxValue)
        {            
            SceneManager.LoadScene((int)Scene);
        }
    }
	
}
