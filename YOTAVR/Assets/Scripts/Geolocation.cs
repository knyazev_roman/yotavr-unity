﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Geolocation : MonoBehaviour {

    public Text TextField;
    public GameObject UIInfo;
    public GameObject Cities;
    public ScenesData.SCENES Scene;

    // Wait until service initializes
    public int maxWait = 10;

    IEnumerator Start()
    {
        TextField.text = "RUN!";
        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
        {
            ShowCities();
            yield break; 
        }

        // Start service before querying location
        Input.location.Start();

        
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            print("Timed out");
            TextField.text = "Timed out";
            ShowCities();
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            print("Unable to determine device location");
            TextField.text = "Unable to determine device location";
            ShowCities();
            yield break;
        }
        else
        {
            var t = "Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp;
            TextField.text = t;
            // Access granted and location value could be retrieved
            print(t);
            TextField.text = "DONE-2";
            FindObjectOfType<GetCity>().GetCityName(Input.location.lastData.latitude, Input.location.lastData.longitude);
        }

        // Stop service if there is no need to query location updates continuously
        Input.location.Stop();
    }

    public void ShowCities()
    {
        UIInfo.SetActive(false);
        Cities.SetActive(true);
    }
}
