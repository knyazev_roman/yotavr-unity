﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerStepObjects : MonoBehaviour {

    public StepObject[] objs;
    public int activeStep = 0;
	void Start () {
        objs[activeStep].Show();
    }
	void Update () {
		
        if(objs.Length> activeStep){
            if(objs[activeStep].Finish)
            {
                objs[activeStep].gameObject.SetActive(false);
                activeStep++;
                objs[activeStep].gameObject.SetActive(true);
                objs[activeStep].Show();
            }
        }
	}
}
