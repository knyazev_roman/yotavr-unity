﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdvantageStepByStep : MonoBehaviour {

    public DrawUnlimited Step1Infiniti;
    public DrawMap Step2Map;
    //public DrawMap Step2Map;
    public Compas Step3Compas;


    public GameObject Step1;
    public AudioSource AudioStep1;
    public MonoBehaviour[] Step1Gamovjects;
    public GameObject Step2;
    public AudioSource AudioStep2;
    public MonoBehaviour[] Step2Gamovjects;

    public GameObject Step3;
    public AudioSource AudioStep3;
    public MonoBehaviour[] Step3Gamovjects;
    public CompasDone Compdone;

    // Use this for initialization
    void Start () {
        if (Step1Infiniti == null) Step1Infiniti = GameObject.FindObjectOfType<DrawUnlimited>();
        //CanvasRenderer[] ChildrenRenderer = ContinerCanvasImages.GetComponentsInChildren(typeof(CanvasRenderer)) as CanvasRenderer[];
        //foreach (var item in ChildrenRenderer){
        //    Debug.Log(item.name);
        //}
    }
    public bool isDebug=false;
    float t = 1;

    public bool DebugStep2 = false;
    bool isStep1 = false;
    bool isStep2 = false;
    bool isStep3 = false;
    public bool DebugStep3 = false;

    //void Update()
    //{
    //    #region Step1
    //    if (isStep1 == false)
    //    {
    //        if (Step1Infiniti.IsDone /*&& Step1Infiniti.FastLoad */|| (isDebug))
    //        {
    //            t = t - (0.173f * Time.deltaTime);
    //            foreach (var item in Step1Infiniti.UiHides)
    //            {

    //                item.GetComponent<Image>().color = new Color(
    //                item.GetComponent<Image>().color.r,
    //                item.GetComponent<Image>().color.g,
    //                item.GetComponent<Image>().color.b,
    //                t
    //                );
    //                if (t < 0)
    //                {
    //                    t = 0;
    //                    isStep1 = true;
    //                    foreach (MonoBehaviour itemT in Step1Gamovjects)
    //                    {
    //                        itemT.enabled = false;
    //                    }
    //                    AudioStep1.enabled = false;
    //                    Step1.SetActive(false);
    //                    Step2.SetActive(true);
    //                    foreach (MonoBehaviour itemT in Step2Gamovjects)
    //                    {
    //                        itemT.enabled = true;
    //                    }
    //                    AudioStep2.enabled = true;
    //                    DebugStep2 = true;
    //                }
    //            }
    //        }
    //    }
    //    #endregion
    //}
    //    void Update123()
    //{
    //    #region Step1
    //    if (isStep1 == false)
    //    {
    //        if (Step1Infiniti.IsDone /*&& Step1Infiniti.FastLoad */|| (isDebug))
    //        {
    //            t = t - (0.173f * Time.deltaTime);
    //            foreach (var item in Step1Infiniti.UiHides)
    //            {

    //                item.GetComponent<Image>().color = new Color(
    //                item.GetComponent<Image>().color.r,
    //                item.GetComponent<Image>().color.g,
    //                item.GetComponent<Image>().color.b,
    //                t
    //                );
    //                if (t < 0){
    //                    t = 0;
    //                    isStep1 = true;
    //                    foreach (MonoBehaviour itemT in Step1Gamovjects)
    //                    {
    //                        itemT.enabled = false;
    //                    }
    //                    AudioStep1.enabled = false;
    //                    Step1.SetActive(false);
    //                    Step2.SetActive(true);
    //                    foreach (MonoBehaviour itemT in Step2Gamovjects)
    //                    {
    //                        itemT.enabled = true;
    //                    }
    //                    AudioStep2.enabled = true;
    //                    DebugStep2 = true;
    //                }
    //            }


    //        }
    //    }
    //    #endregion
    //    if(DebugStep2)
    //    {
    //        t = t + (0.173f * Time.deltaTime);
    //        foreach (var item in Step2Map.UiHides)
    //        {

    //            item.GetComponent<Image>().color = new Color(
    //            item.GetComponent<Image>().color.r,
    //            item.GetComponent<Image>().color.g,
    //            item.GetComponent<Image>().color.b,
    //            t
    //            );
    //        }
    //        if (t >= 1)
    //        {
    //            DebugStep2 = false;
    //        }
    //    }
    //    #region Step2
    //    if (isStep2 == false)
    //    {
    //        if (Step2Map.IsDone /*&& Step1Infiniti.FastLoad */)
    //        {
    //            t = t - (0.173f * Time.deltaTime);
    //            foreach (var item in Step2Map.UiHides)
    //            {

    //                item.GetComponent<Image>().color = new Color(
    //                item.GetComponent<Image>().color.r,
    //                item.GetComponent<Image>().color.g,
    //                item.GetComponent<Image>().color.b,
    //                t
    //                );
    //                if (t < 0)
    //                {
    //                    t = 0;
    //                    isStep2 = true;
    //                    foreach (MonoBehaviour itemT in Step2Gamovjects)
    //                    {
    //                        itemT.enabled = false;
    //                    }
    //                    AudioStep2.enabled = false;
    //                    Step2.SetActive(false);

    //                    Step3.SetActive(true);
    //                    foreach (MonoBehaviour itemT in Step3Gamovjects)
    //                    {
    //                        itemT.enabled = true;
    //                    }
    //                    AudioStep3.enabled = true;
    //                    DebugStep3 = true;
    //                }
    //            }


    //        }
    //    }
    //    #endregion
    //    if (DebugStep3)
    //    {
    //        t = t + (0.173f * Time.deltaTime);
    //        foreach (var item in Step3Compas.UiHides)
    //        {
    //            item.color = new Color(
    //            item.color.r,
    //            item.color.g,
    //            item.color.b,
    //            t
    //            );
    //        }
    //        if (t >= 1)
    //        {
    //            DebugStep3 = false;
    //        }
    //    }

    //    if (isStep3 == false)
    //    {
    //        if (Compdone.IsDone /*&& Step1Infiniti.FastLoad */)
    //        {
    //            t = t - (0.173f * Time.deltaTime);
    //            foreach (var item in Step3Compas.UiHides)
    //            {

    //                item.color = new Color(
    //                item.color.r,
    //                item.color.g,
    //                item.color.b,
    //                t
    //                );

    //                if (t < 0)
    //                {
    //                    isStep3 = true;
    //                    //t = 0;
    //                    //isStep2 = true;
    //                    //foreach (MonoBehaviour itemT in Step2Gamovjects)
    //                    //{
    //                    //    itemT.enabled = false;
    //                    //}
    //                    //AudioStep2.enabled = false;
    //                    //Step2.SetActive(false);

    //                    //Step3.SetActive(true);
    //                    //foreach (MonoBehaviour itemT in Step3Gamovjects)
    //                    //{
    //                    //    itemT.enabled = true;
    //                    //}
    //                    //AudioStep3.enabled = true;
    //                    //DebugStep3 = true;
    //                }
    //            }
    //        }
    //    }

    //}

    // Update is called once per frame
    //   void Update123 () {
    //	if(Step1Infiniti.IsDone&& Step1Infiniti.FastLoad)
    //       {
    //           foreach (var item in Step1Infiniti.UiHides)
    //           {
    //              var d =  item.GetComponent<CanvasRenderer>().GetMaterial();
    //              // d.color = 
    //           }
    //       }
    //}
}
