﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainViewer : MonoBehaviour {

    private Animation animation;

    // Use this for initialization
    void Start()
    {
        animation = GetComponent<Animation>();
        animation.wrapMode = WrapMode.Once;
    }

    // Update is called once per frame
    void Update()
    {
    }
}
