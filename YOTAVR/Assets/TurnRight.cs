﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnRight : MonoBehaviour {

    public float timer = 2f;

    public GameObject turnRight;

	void Start () {
	}
	
	void Update () {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            turnRight.SetActive(true);
        }
	}
}
