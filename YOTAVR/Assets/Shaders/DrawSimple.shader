﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/DrawSimple" {

	SubShader 
    {
        ZWrite Off
        ZTest Always
        Lighting Off

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
 
            struct v2f
            {
                float4 vertex : POSITION;
            };
 
            //just get the position correct
            v2f vert(v2f i)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(i.vertex);
                return o;
            }
 
            //return white
            half4 frag() : COLOR0
            {
                return half4(1,1,1,1);
            }
 
            ENDCG
        }
    }
}
