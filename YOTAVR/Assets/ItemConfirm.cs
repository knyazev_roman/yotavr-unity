﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemConfirm : MonoBehaviour {

    public ScenesData.SCENES Scene;
    // Use this for initialization
    void Start () {
		
	}
    private bool IsLoad = false;
    private AsyncOperation operation;
    public void Loadlevel()
    {
        if (IsLoad) return;
        IsLoad = true;
        //StartCoroutine(LoadScene());

        UnityEngine.SceneManagement.SceneManager.LoadScene((int)Scene/*, UnityEngine.SceneManagement.LoadSceneMode.Single*/);

    }

    private IEnumerator LoadScene()
    {
        operation = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync((int)Scene/*, UnityEngine.SceneManagement.LoadSceneMode.Single*/);
        operation.allowSceneActivation = false;
        yield return operation;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
