﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioControllerYota : MonoBehaviour {

    public AudioSource SourceAudio;
    public AudioClip[] Clips;


	// Use this for initialization
	void Start () {
        if (SourceAudio == null)
            SourceAudio = GetComponent<AudioSource>();
    }

    int i = 0;
	void Update () {
        if (Clips != null && Clips.Length > 0 && i < Clips.Length)
        {
            if (!SourceAudio.isPlaying)
            {
                var audioSources = FindObjectsOfType<AudioSource>() as AudioSource[];
                foreach (AudioSource item in audioSources)
                {
                    item.Stop();
                }
                SourceAudio.PlayOneShot(Clips[i]);
                i++;
            }
        }
	}

    public void StartPlayAudio(AudioClip clip)
    {
        SourceAudio.Stop();
        SourceAudio.PlayOneShot(clip);
    }

}
