﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeViewer : MonoBehaviour {

    public Animation anim;
    public float timer;
    private bool hasPlayed = false;

	// Use this for initialization
	void Start () {
        anim.wrapMode = WrapMode.Once;
	}
	
	// Update is called once per frame
	void Update () {
        if (timer > -1)
            timer -= Time.deltaTime;

        if (timer <= 0 && !hasPlayed)
        {
            anim.Play();
            hasPlayed = true;
        }
	}
}
