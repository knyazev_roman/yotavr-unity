﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoNotDestoy : MonoBehaviour {
    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }
}
