﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SocialActivate : MonoBehaviour {
	public Sprite HoveredSprite;
	public Sprite DisabledSprite;
	public Button SocialButton;
	public GameObject CloudMessage;

	private bool _buttonEnabled = false;
	private Image _buttonImage;

	public void Start ()
	{
	}

	public void Update ()
	{
		
	}

	public void ChangeImage ()
	{
		_buttonImage = SocialButton.GetComponent<Image> ();
		if (_buttonEnabled)
			_buttonImage.sprite = DisabledSprite;
		else
			_buttonImage.sprite = HoveredSprite;
	}

}
