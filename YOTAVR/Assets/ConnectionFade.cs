﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum fadeStates
{
    FADE_IN = 0,
    SHOWING,
    FADE_OUT
};

public class ConnectionFade : MonoBehaviour {

    private RectTransform rectTransform;
    private Vector3 scale;
    private float fadeIn, fadeOut;
    private float time;

    private fadeStates state, lastState;

    private static fadeStates globalState;
    private static bool upd = false;


    void Start () {
        rectTransform = GetComponent<RectTransform>();
        scale = rectTransform.localScale;
        fadeIn = Random.Range(.3f, .6f);
        fadeOut = Random.Range(.4f, .8f);

        rectTransform.localScale = new Vector3(0, 0, 0);
        state = lastState = fadeStates.FADE_IN;
        time = Time.time;
    }
	
	void Update () {
        if (upd)
        {
            state = globalState;
        }

        if (state != lastState)
        {
            time = Time.time;
            lastState = state;
        }

        if (state == fadeStates.FADE_IN)
        {
            float delta = (Time.time - time) / fadeIn;
            if (delta < 1f)
            {
                rectTransform.localScale = new Vector3(scale.x * delta, scale.y * delta, scale.z * delta);
            }
            else
            {
                state = fadeStates.SHOWING;
                rectTransform.localScale = scale;
            }
        }
        if (state == fadeStates.FADE_OUT)
        {
            float delta = 1 - (Time.time - time) / fadeOut;
            if (delta > 0f)
            {
                rectTransform.localScale = new Vector3(scale.x * delta, scale.y * delta, scale.z * delta);
            }
            else
            {
                state = fadeStates.SHOWING;
                rectTransform.localScale = new Vector3(0, 0, 0);
            }
        }

        upd = false;
    }

    public static void SetGlobalState(fadeStates state)
    {
        globalState = state;
        upd = true;
    }
}
