﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SessionLog : MonoBehaviour {
    public static string session_code = "";
    string url = "https://sa-wd.ru/yota/GetSessionWork";

    private void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
        StartCoroutine(Parse());
    }

    // Use this for initialization
    void Start () {

    }

    IEnumerator Parse()
    {
        WWW www = new WWW(url);
        yield return www;
        if (www.error == null)
        {
            var N = SimpleJSON.JSON.Parse(www.text);
            var code = N["session"].Value;
            session_code = code;
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
