﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class VRBuildSettings : MonoBehaviour {

    [MenuItem("VR Build Settings/Build GearVR")]
    static void BuildGearVRSettings()
    {
        var Touch = FindObjectOfType<TouchDraw>();
        if(Touch)
            Touch.NeedTouch = true;
        PlayerSettings.virtualRealitySupported = true;
        var GvrViewerItem = FindObjectOfType<GvrViewer>();
        GvrViewerItem.VRModeEnabled = false;
        var Head = FindObjectOfType<GvrHead>();
        //Head.trackPosition = false;
        Head.trackRotation = false;
        EditorUtility.SetDirty(GvrViewerItem);
        EditorUtility.SetDirty(Head);
    }

    [MenuItem("VR Build Settings/Build Cardboard")]
    static void BuildCardboardVRSettings()
    {
        var Touch = FindObjectOfType<TouchDraw>();
        if (Touch)
            Touch.NeedTouch = false;
        PlayerSettings.virtualRealitySupported = false;
        var GvrViewerItem = FindObjectOfType<GvrViewer>();
        GvrViewerItem.VRModeEnabled = true;
        var Head = FindObjectOfType<GvrHead>();
        //Head.trackPosition = false;
        Head.trackRotation = true;
        EditorUtility.SetDirty(GvrViewerItem);
        EditorUtility.SetDirty(Head);
    }

}
